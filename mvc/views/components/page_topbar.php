        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="<?=base_url('dashboard/index'); ?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <?php if(inicompute($siteinfos)) { echo namesorting($siteinfos->sname, 14); } ?>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>


                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown messages-menu my-push-message">
                            <a href="#" class="dropdown-toggle my-push-message-a" data-toggle="dropdown" >
                                <i class="fa fa-bell-o" ></i>
                            </a>
                            <ul class="dropdown-menu my-push-message-ul" style="display:none">
                                <li class='header my-push-message-number'>
                                </li>
                                <li>
                                    <ul class="menu my-push-message-list">
                                    </ul>
                                </li>
                            </ul>
                        </li>

						<!--
                        <?php if(isset($siteinfos->language_status) && $siteinfos->language_status == 0) { ?>

                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img class="language-img" src="<?php 
                                $image = $this->session->userdata('lang'); 
                                echo base_url('uploads/language_image/'.$image.'.png'); ?>" 
                                /> 
                                <span class="label label-warning">2</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"> <?=$this->lang->line("language")?></li>
                                <li>

                                    <ul class="menu">

                                        <li class="language" id="english">
                                            <a href="<?=base_url('language/index/english')?>">
                                                <div class="pull-left">
                                                    <img src="<?=base_url('uploads/language_image/english.png'); ?>"/>
                                                </div>
                                                <h4>
                                                    English
                                                    <?php if($image == 'english') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
                                                </h4>
                                            </a>
                                        </li>

                                        <li class="language" id="indonesian">
                                            <a href="<?=base_url('language/index/indonesian')?>">
                                                <div class="pull-left">
                                                    <img src="<?=base_url('uploads/language_image/indonesian.png'); ?>"/>
                                                </div>
                                                <h4>
                                                    Indonesian
                                                    <?php if($image == 'indonesian') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
                                                </h4>
                                            </a>
                                        </li>

             
                                    </ul>
                                </li>
                                <li class="footer"></li>
                            </ul>
                        </li>
                        <?php } ?>
						-->
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?=imagelink($this->session->userdata('photo'))?>" class="user-logo" alt="" />
                                <span>
                                    <?php
                                        $name = $this->session->userdata('name');
                                        if(strlen($name) > 11) {
                                           echo substr($name, 0,11). ".."; 
                                        } else {
                                            echo $name;
                                        }
                                    ?>
                                    <i class="caret"></i>
                                </span>   
                            </a>

                            <ul class="dropdown-menu">
                                <li class="user-body">
                                    <div class="col-xs-6 text-center">
									<?php 
									$usertypeID = $this->session->userdata('usertypeID');
									if($usertypeID == 3) { ?>
                                        <a href="<?=base_url("profile/index")?>">
                                            <div><i class="fa fa-briefcase"></i></div>
                                            <?=$this->lang->line("profile")?> 
                                        </a>
									<?php } else { ?>
										<a href="<?=base_url("profile/index")?>">
                                            <div><i class="fa fa-briefcase"></i></div>
                                            <?=$this->lang->line("profile")?> 
                                        </a>
									<?php } ?>
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        <a href="<?=base_url("signin/cpassword")?>">
                                            <div><i class="fa fa-lock"></i></div>
                                            <?=$this->lang->line("change_password")?> 
                                        </a>
                                    </div>
                                </li>

                                <li class="user-footer">
                                    <div class="text-center">
                                        <a href="<?=base_url("signin/signout")?>">
                                            <div><i class="fa fa-power-off"></i></div>
                                            <?=$this->lang->line("logout")?> 
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>