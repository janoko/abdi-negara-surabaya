        </div><!-- ./wrapper -->

        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/bootstrap.min.js'); ?>"></script>
        <!-- Style js -->
        <script type="text/javascript" src="<?php echo base_url('assets/inilabs/style.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/tinymce4/tinymce.min.js'); ?>"></script>
        <!-- Jquery datatable tools js -->
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/tools/jquery.dataTables.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/tools/dataTables.buttons.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/tools/jszip.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/tools/pdfmake.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/tools/vfs_fonts.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/tools/buttons.html5.min.js'); ?>"></script>
        <!-- dataTables Tools / -->
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js'); ?>"></script>

        <script type="text/javascript" src="<?php echo base_url('assets/inilabs/inilabs.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/tinymce4/tinymce.min.js'); ?>"></script>
		 <script type="text/javascript" src="<?php echo base_url('assets/wiris/wirislib.js'); ?>"></script>
		<script>
		tinymce.init({
			selector: 'textarea.tinymceeditor',
			menubar : false,
			plugins: 'fullscreen,code,preview,autolink,table,tiny_mce_wiris,image, autolink lists link image charmap print, paste textcolor colorpicker textpattern',
			fullscreen_native: true,
			toolbar: 'fontsizeselect | bold,italic,underline,|,search,|,undo,redo,| alignleft aligncenter alignright alignjustify | table insertfile  link image responsivefilemanager | tiny_mce_wiris_formulaEditor, tiny_mce_wiris_formulaEditorChemistry | code, fullscreen',
			setup : function(ed)
			{
				ed.on('init', function()
				{
					
				});
			},
			
			height : "230",
			// Disable branding message, remove "Powered by TinyMCE"
			branding: false, 
			wirisimagebgcolor: '#FFFFFF',
			wirisimagesymbolcolor: '#000000',
			wiristransparency: 'true',
			wirisimagefontsize: '33',
			wirisimagenumbercolor: '#000000',
			wirisimageidentcolor: '#000000',
			cleanup : false,
			verify_html : false,
			automatic_uploads: true,
			image_advtab: true,
			images_upload_url: "<?php echo base_url('dashboard/upload')?>",
			file_picker_types: 'image', 
			paste_data_images:true,
			relative_urls: false,
			remove_script_host: false,
			file_picker_callback: function(cb, value, meta) {
			 var input = document.createElement('input');
			 input.setAttribute('type', 'file');
			 input.setAttribute('accept', 'image/*');
			 input.onchange = function() {
				var file = this.files[0];
				var reader = new FileReader();
				reader.readAsDataURL(file);
				reader.onload = function () {
				   var id = 'post-image-' + (new Date()).getTime();
				   var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
				   var blobInfo = blobCache.create(id, file, reader.result);
				   blobCache.add(blobInfo);
				   cb(blobInfo.blobUri(), { title: file.name });
				};
			 };
			 input.click();
		  }
		});
		</script>
			
        <script>
            $(document).ready(function() {
                $('#example3, #example1, #example2').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],
                    search: false
                });
            });
        </script>

        <script type="text/javascript">
            $(function() {
                $("#withoutBtn").dataTable();
            });
        </script>

        <?php if ($this->session->flashdata('success')): ?>
            <script type="text/javascript">
                toastr["success"]("<?=$this->session->flashdata('success');?>")
                toastr.options = {
                    "closeButton": true,
                    "allowHtml": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "500",
                    "hideDuration": "500",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            </script>
        <?php endif ?>
        <?php if ($this->session->flashdata('error')): ?>
           <script type="text/javascript">
                toastr["error"]("<?=$this->session->flashdata('error');?>")
                toastr.options = {
                    "closeButton": true,
                    "allowHtml": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "500",
                    "hideDuration": "500",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            </script>
        <?php endif ?>

        <?php
            if(isset($footerassets)) {
                foreach ($footerassets as $assetstype => $footerasset) {
                    if($assetstype == 'css') {
                        if(inicompute($footerasset)) {
                            foreach ($footerasset as $keycss => $css) {
                                echo '<link rel="stylesheet" href="'.base_url($css).'">'."\n";
                            }
                        }
                    } elseif($assetstype == 'js') {
                        if(inicompute($footerasset)) {
                            foreach ($footerasset as $keyjs => $js) {
                                echo '<script type="text/javascript" src="'.base_url($js).'"></script>'."\n";
                            }
                        }
                    }
                }
            }
        ?>

        <script type="text/javascript">
            $("ul.sidebar-menu li").each(function(index, value) {
                if($(this).attr('class') == 'active') {
                    $(this).parents('li').addClass('active');
                }
            });

            $(document).ready(function () {
                setTimeout(function () {
                    $.ajax({
                        type : 'GET',
                        dataType : "html",
                        url : "<?=base_url('alert/alert')?>",
                        success : function (data) {
                            $(".my-push-message-list").html(data);
                            var alertNumber = 0;
                            $('.my-push-message-list li').each(function () {
                                alertNumber++;
                            });
                            if (alertNumber > 0) {
                                $('.my-push-message-ul').removeAttr('style');
                                $('.my-push-message-a').append('<span class="label label-danger"><lable class="alert-image">' + alertNumber + '</lable> </span>');
                                $('.my-push-message-number').html('<?=$this->lang->line("la_fs") . " "?>' + alertNumber + '<?=" " . $this->lang->line("la_ls")?>');
                            } else {
                                $('.my-push-message-ul').remove();
                            }
                        }
                    });
                }, 10000);
          });
		 
		
      </script>
	  
    </body>
</html>
