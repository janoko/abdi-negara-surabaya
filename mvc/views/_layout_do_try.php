<?php 
$uripage = $this->uri->segment(2, 'Nothing');  
$usertypeID = 280;
?>
<?php $this->load->view("components/page_header"); ?>

<?php if($usertypeID == 1 || $usertypeID == 2 || $uripage == 'index') { ?>
	<?php $this->load->view("components/page_topbar"); ?>
	<?php $this->load->view("components/page_menu"); ?>
<?php } else { ?>
	<?php if($uripage == 'Nothing' && ($uripage != 'show' || $uripage != 'instruction') ) { ?>
		<?php $this->load->view("components/page_topbar"); ?>
		<?php $this->load->view("components/page_menu"); ?>
	<?php } ?>
<?php } ?>

	
		<?php if($usertypeID == 1 || $usertypeID == 2 || $uripage == 'index') { ?>
			<aside class="right-side">
		<?php } else { ?>
			<?php if($uripage == 'Nothing' && ($uripage != 'show' || $uripage != 'instruction') ) { ?>
				<aside class="right-side">
			<?php } ?>
		<?php } ?>
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <?php $this->load->view($subview); ?>
                    </div>
                </div>
            </section>
		<?php if($usertypeID == 1 || $usertypeID == 2 || $uripage == 'index') { ?>
			</aside>
		<?php } else { ?>
			<?php if($uripage == 'Nothing' && ($uripage != 'show' || $uripage != 'instruction') ) { ?>
				</aside>
			<?php } ?>
		<?php } ?>
		
<?php if($usertypeID == 1 || $usertypeID == 2 || $uripage == 'index') { ?>
			<footer class="main-footer">
          	<div class="pull-right hidden-xs">
            	 Support by <b><a target="_blank" href="https://www.divasoft.net/jasa-pembuatan-website-murah-surabaya">divasoft.net</a></b>
				
          	</div>
          	<strong><?=$siteinfos->footer?></strong>
        </footer>
		<?php } else { ?>
			<?php if($uripage == 'Nothing' && ($uripage != 'show' || $uripage != 'instruction') ) { ?>
				<footer class="main-footer">
          	<div class="pull-right hidden-xs">
            	 Support by <b><a target="_blank" href="https://www.divasoft.net/jasa-pembuatan-website-murah-surabaya">divasoft.net</a></b>
				
          	</div>
          	<strong><?=$siteinfos->footer?></strong>
        </footer>

			<?php } ?>
		<?php } ?>
		
<?php $this->load->view("components/page_footer"); ?>
