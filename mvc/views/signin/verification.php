
<div class="form-box" id="login-box" style="width: 450px;">
    <div class="header">Verifikasi</div>
    <form method="post">

        <!-- style="margin-top:40px;" -->
        <div class="body white-bg">
        <a class="btn btn-light btn-sm" href="index" style="margin: 0px;"><- Back to Login Page</a>

        <?php 
        if($verification_error === FALSE) { ?>

            <div class="alert alert-info alert-dismissable" style="margin: 0px;">
                Kami telah mingirimkan kode verifikasi pada Email anda<?= (isset($hiddenEmail))? "(".$hiddenEmail.")": ""?>. 
                Silahkan cek inbox. Atau coba cek di folder spam.
                Jika tidak ada silahkan <a class="btn btn-default btn-xs" style="margin: 0px;">kirim lagi</a>.
            </div>

        <?php 
        } else if ($verification_error == "NoEmail"){ ?>

            <div class="alert alert-danger alert-dismissable" style="margin: 0px;">
                Kami tidak dapat mengirimkan kode verifikasi ke email anda karena email belum disetting. 
                Silahkan menghubungi admin untuk menambahkan email ke akun anda. 
                Atau anda bisa verifikasi menggunakan pin.
            </div>

        <?php 
        } else if ($verification_error == "LimitedGenerate"){ ?>

            <div class="alert alert-danger alert-dismissable" style="margin: 0px;">
                Limit/batas generate kode menggunakan email hanya 2 kali dalam sehari.
                Silahkan verifikasi menggunakan pin.
            </div>

        <?php
        } else if ($verification_error == "WrongCode"){ ?>

            <div class="alert alert-danger alert-dismissable" style="margin: 0px;">
                Kode verifikasi salah. 
            </div>

        <?php
        }
        if($this->session->flashdata('reset_success')) {
            $message = $this->session->flashdata('reset_success');
            echo "<div class=\"alert alert-success alert-dismissable\">
                <i class=\"fa fa-ban\"></i>
                <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                $message
            </div>";
        }
        ?>
            <div class="form-group input-group-lg">
                <input class="form-control" placeholder="Kode Verifikasi" name="verification_code" type="text" autofocus value="">
            </div>
            
            <div class="form-group row justify-content-md-center" style="margin: 0px;">
                <a class="btn btn-default btn-lg col-sm-6" href="verification_by_pin" style="margin: 0px;">Verify by Pin</a>
                <input type="submit" class="btn btn-lg btn-success col-sm-6" value="Verifikasi" />
            </div>
        </div>
    </form>
</div>
