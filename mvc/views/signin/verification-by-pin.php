
<div class="form-box" id="login-box" style="width: 450px;">
    <div class="header">Verifikasi dengan Pin</div>
    <form method="post">

        <!-- style="margin-top:40px;" -->
        <div class="body white-bg">
        <a class="btn btn-light btn-sm" href="index" style="margin: 0px;"><- Back to Login Page</a>
        <?php
            if($form_validation == "No"){
        ?>
            <!-- <div class="alert alert-info alert-dismissable" style="margin: 0px;">
                Kami telah mingirimkan kode verifikasi pada Email anda (gh****@gmail.com). Silahkan cek inbox. Atau coba cek di folder spam.
                Jika tidak ada silahkan <a class="btn btn-default btn-xs" style="margin: 0px;">kirim lagi</a>.
            </div> -->
        <?php } else {
                if(inicompute($form_validation)) {
                    echo "<div class=\"alert alert-danger alert-dismissable\">
                        <i class=\"fa fa-ban\"></i>
                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                        $form_validation
                    </div>";
                }
            }
            if($this->session->flashdata('reset_success')) {
                $message = $this->session->flashdata('reset_success');
                echo "<div class=\"alert alert-success alert-dismissable\">
                    <i class=\"fa fa-ban\"></i>
                    <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                    $message
                </div>";
            }
        ?>
            <div class="form-group input-group-lg">
                <input class="form-control" placeholder="Kode Pin" name="kodepin" type="text" autofocus value="">
            </div>
            
            <input type="submit" class="btn btn-lg btn-success btn-block" value="Verifikasi" />
        </div>
    </form>
</div>
