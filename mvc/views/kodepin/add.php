
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-sitemap"></i> <?=$this->lang->line('panel_title')?></h3>


        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("kodepin/index")?>"></i> <?=$this->lang->line('menu_kodepin')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_kodepin')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post">

                    <?php
                        if(form_error('kodepin'))
                            echo "<div class='form-group has-error' >";
                        else
                            echo "<div class='form-group' >";
                    ?>
                        <label for="kodepin" class="col-sm-2 control-label">
                            <?=$this->lang->line("kodepin_name")?> <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="kodepin" name="kodepin" value="<?=set_value('kodepin')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('kodepin'); ?>
                        </span>
                    </div>
					
					 <?php
                    if(form_error('quota'))
                        echo "<div class='form-group has-error' id='quotaDiv'>";
                    else
                        echo "<div class='form-group' id='quotaDiv'>";
                    ?>
                        <label for="quota" class="col-sm-2 control-label" >
                            <?=$this->lang->line("kodepin_select")?>
                        </label>
                        <div class="col-sm-6">
                            <?php
                            $array = array(0 => $this->lang->line("kodepin_select"));
                            foreach (range(0,10) as $i) {
                                $array[$i] = $i;
                            }
                            echo form_dropdown("quota", $array, set_value("quota"), "id='quota' class='form-control select2'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('quota'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_kodepin")?>" >
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

