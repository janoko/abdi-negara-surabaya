
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-sitemap"></i> <?=$this->lang->line('panel_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <?php if($siteinfos->school_type == 'classbase') { ?>
                <li class="active"><?=$this->lang->line('menu_kodepin')?></li>
            <?php } else { ?>
                <li class="active"><?=$this->lang->line('menu_department')?></li>
            <?php } ?>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php 
                    $usertype = $this->session->userdata("usertype");
                    if(permissionChecker('kodepin_add')) {
                ?>
                    <h5 class="page-header">
                        <a href="<?php echo base_url('kodepin/add') ?>">
                            <i class="fa fa-plus"></i> 
                            <?=$this->lang->line('add_title')?>
                        </a>
                    </h5>
                <?php } ?>

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-lg-1"><?=$this->lang->line('slno')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('kodepin_name')?></th>
                                <th class="col-lg-2">Quota</th>
								 <th class="col-sm-1"><?=$this->lang->line('kodepin_status')?></th>
                                 <?php if(permissionChecker('kodepin_edit') || permissionChecker('kodepin_delete')) { ?>
                                <th class="col-lg-2"><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(inicompute($kodepin)) {$i = 1; foreach($kodepin as $kodepin) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('kodepin_name')?>">
                                        <?php echo $kodepin->kodepin; ?>
                                    </td>
									 <td data-title="Quota">
                                        <?php echo $kodepin->quota; ?>
                                    </td>
									<?php if(permissionChecker('kodepin_edit')) { ?>
									<td data-title="<?=$this->lang->line('kodepin_status')?>">
										<div class="onoffswitch-small" id="<?=$kodepin->kodepinID?>">
											<input type="checkbox" id="myonoffswitch<?=$kodepin->kodepinID?>" class="onoffswitch-small-checkbox" name="paypal_demo" <?php if($kodepin->status === '1') echo "checked='checked'"; ?>>
											<label for="myonoffswitch<?=$kodepin->kodepinID?>" class="onoffswitch-small-label">
												<span class="onoffswitch-small-inner"></span>
												<span class="onoffswitch-small-switch"></span>
											</label>
										</div>
									</td>
									<?php } ?>
                                    <?php if(permissionChecker('kodepin_edit') || permissionChecker('kodepin_delete')) { ?>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_edit('kodepin/edit/'.$kodepin->kodepinID, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('kodepin/delete/'.$kodepin->kodepinID, $this->lang->line('delete')) ?>
                                    </td>
                                    <?php } ?>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var status = '';
    var id = 0;
    $('.onoffswitch-small-checkbox').click(function() {
        if($(this).prop('checked')) {
            status = 'chacked';
            id = $(this).parent().attr("id");
        } else {
            status = 'unchacked';
            id = $(this).parent().attr("id");
        }

        if((status != '' || status != null) && (id !='')) {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('kodepin/active')?>",
                data: "id=" + id + "&status=" + status,
                dataType: "html",
                success: function(data) {
                    if(data == 'Success') {
                        toastr["success"]("Success")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "500",
                            "hideDuration": "500",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    } else {
                        toastr["error"]("Error")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "500",
                            "hideDuration": "500",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                }
            });
        }
    });
</script>
