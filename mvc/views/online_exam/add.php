
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-slideshare"></i> <?=$this->lang->line('panel_title')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("online_exam/index")?>"><?=$this->lang->line('menu_online_exam')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_online_exam')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post">
                    <?php
                    if(form_error('name'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="name" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_name")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name')?>">
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('name'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('usertype'))
                        echo "<div class='form-group has-error' style='display:none;'>";
                    else
                        echo "<div class='form-group' style='display:none;'>";
                    ?>
                        <label for="usertype" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_usertype")?>
                        </label>
                        <div class="col-sm-6">
                            <?php
                            $array = array(0 => $this->lang->line("online_exam_select"));
                            if(inicompute($usertypes)) {
                                foreach ($usertypes as $usertype) {
                                    $array[$usertype->usertypeID] = $usertype->usertype;
                                }
                            }
                            echo form_dropdown("usertype", $array, set_value("usertype", $userTypeID), "id='usertype' class='form-control select2'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('usertype'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('classes'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="classes" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_class")?>
                        </label>
                        <div class="col-sm-6">
                            <div class="select2-wrapper">
                                <?php
                                $array = array();
                                if(inicompute($classes)) {
                                    foreach ($classes as $class) {
                                        $array[$class->classesID] = $class->classes;
                                    }
                                }
                                echo form_dropdown("classes[]", $array, get_instance()->input->post("classes", FALSE), "id='classes' class='form-control select2' multiple='multiple'");
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    if(form_error('sections'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="section" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_section")?>
                        </label>
                        <div class="col-sm-6">
                            <div class="select2-wrapper">
                                <?php
                                echo form_dropdown("sections[]", $sections_option, get_instance()->input->post("sections", FALSE), "id='section' class='form-control select2' multiple='multiple'");
                                ?>
                            </div>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('sections'); ?>
                        </span>
                    </div>
					
					<?php
                    if(form_error('jenis'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="jenis" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_jenis")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <?php
                                $arrayJenis = array(0 => $this->lang->line("online_exam_select"));
                                if(inicompute($jenis)) {
                                    foreach ($jenis as $jeni) {
                                        $arrayJenis[$jeni->examJenisNumber] = $jeni->title;
                                    }
                                }
                                echo form_dropdown("jenis", $arrayJenis, set_value("jenis"), "id='jenis' class='form-control select2'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('jenis'); ?>
                        </span>
                    </div>
                    
                    <?php
                    if(form_error('jenis_text'))
                        echo "<div class='form-group has-error' id='jenis_text_field'>";
                    else
                        echo "<div class='form-group' id='jenis_text_field'>";
                    ?>
                        <label for="name" class="col-sm-2 control-label">
                            <!-- Nama Jenis
                            <span class="text-red">*</span> -->
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="jenis_text" name="jenis_text" value="<?=set_value('jenis_text')?>" placeholder="Nama Jenis">
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('jenis_text'); ?>
                        </span>
                    </div>
					
					<div id="instructionss">
						<?php
						if(form_error('instruction'))
							echo "<div class='form-group has-error' >";
						else
							echo "<div class='form-group' >";
						?>
							<label for="instruction" class="col-sm-2 control-label">
								<?=$this->lang->line("online_exam_instruction")?>
							</label>
							<div class="col-sm-6">
								<?php
								$arrayInstruction = array(0 => $this->lang->line("online_exam_select"));
								if(inicompute($instructions)) {
									foreach ($instructions as $instruction) {
										$arrayInstruction[$instruction->instructionID] = $instruction->title;
									}
								}
								echo form_dropdown("instruction", $arrayInstruction, set_value("instruction"), "id='instruction' class='form-control select2'");
								?>
							</div>
							<span class="col-sm-4 control-label">
								<?php echo form_error('instruction'); ?>
							</span>
						</div>
                    </div>

                    <?php
                    if(form_error('random'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="random" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_random")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <?php
                                $arrayRand['0'] = "Pilih";
                                $arrayRand['1'] = "Urut";
                                $arrayRand['2'] = "Acak";
                                echo form_dropdown("random", $arrayRand, set_value("random"), "id='random' class='form-control select2'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('random'); ?>
                        </span>
                    </div>
                
                    <div class='form-group' id='is_show_answer_field'>
                        <label for="name" class="col-sm-2 control-label">
                            Tampilkan Jawaban
                        </label>
                        <div class="col-sm-6">
                            <?php
                                $is_show_option = [
                                    "0" => "Tidak",
                                    "1" => "Ya"
                                ];
                                echo form_dropdown("is_show_answer", $is_show_option, set_value("is_show_answer"), "id='is_show_answer_select' class='form-control select2'");
                            ?>
                        </div>
                    </div>
					
					<?php
                    if(form_error('examStatus'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="examStatus" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_exam_status")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <?php
                                $arrayStatus['0'] = $this->lang->line("online_exam_select");
                                $arrayStatus['1'] = $this->lang->line("online_exam_one_time");
                                $arrayStatus['2'] = $this->lang->line("online_exam_multiple_time");
                                echo form_dropdown("examStatus", $arrayStatus, set_value("examStatus"), "id='examStatus' class='form-control select2'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('examStatus'); ?>
                        </span>
                    </div>
					
                    <?php
                    if(form_error('type'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="type" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_type")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <?php
                                $arrayType = array(0 => $this->lang->line("online_exam_select"));
                                if(inicompute($types)) {
                                    foreach ($types as $type) {
                                        $arrayType[$type->examTypeNumber] = $type->title;
                                    }
                                }
                                echo form_dropdown("type", $arrayType, set_value("type"), "id='type' class='form-control select2'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('type'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('duration'))
                        echo "<div class='form-group has-error' id='durationDiv'>";
                    else
                        echo "<div class='form-group' id='durationDiv'>";
                    ?>
                        <label for="duration" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_duration")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="duration" name="duration" value="<?=set_value('duration')?>" placeholder="<?=$this->lang->line("online_exam_minute")?>">
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('duration'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('startdate'))
                        echo "<div class='form-group has-error' id='startdateDiv'>";
                    else
                        echo "<div class='form-group' id='startdateDiv'>";
                    ?>
                        <label for="startdate" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_startdatetime")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="startdate" name="startdate" value="<?=set_value('startdate')?>">
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('startdate'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('enddate'))
                        echo "<div class='form-group has-error' id='enddateDiv'>";
                    else
                        echo "<div class='form-group' id='enddateDiv'>";
                    ?>
                        <label for="enddate" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_enddatetime")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="enddate" name="enddate" value="<?=set_value('enddate')?>">
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('enddate'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('startdatetime'))
                        echo "<div class='form-group has-error' id='startdatetimeDiv'>";
                    else
                        echo "<div class='form-group' id='startdatetimeDiv'>";
                    ?>
                        <label for="startdatetime" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_startdatetime")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="startdatetime" name="startdatetime" value="<?=set_value('startdatetime')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('startdatetime'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('enddatetime'))
                        echo "<div class='form-group has-error' id='enddatetimeDiv'>";
                    else
                        echo "<div class='form-group' id='enddatetimeDiv'>";
                    ?>
                        <label for="enddatetime" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_enddatetime")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="enddatetime" name="enddatetime" value="<?=set_value('enddatetime')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('enddatetime'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('markType'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="markType" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_markType")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <?php
                                $markTypeArray[0]   = $this->lang->line("online_exam_select");
                                $markTypeArray[5]   = $this->lang->line("online_exam_percentage");
                                $markTypeArray[10]  = $this->lang->line("online_exam_fixed");

                                echo form_dropdown("markType", $markTypeArray, set_value("markType"), "id='markType' class='form-control select2'"); 
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('markType'); ?>
                        </span>
                    </div>
					
					<?php
                    if(form_error('negativeMark'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="negativeMark" class="col-sm-2 control-label">
                            Negative Mark
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="negativeMark" name="negativeMark" value="<?=set_value('negativeMark')?>">
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('negativeMark'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('point'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="point" class="col-sm-2 control-label">
                            Point
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="point" name="point" value="<?=set_value('point')?>">
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('point'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('percentage'))
                        echo "<div class='form-group has-error'>";
                    else
                        echo "<div class='form-group'>";
                    ?>
                        <label for="percentage" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_passValue")?> <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="percentage" name="percentage" value="<?=set_value('percentage')?>">
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('percentage'); ?>
                        </span>
                    </div>

                    <?php
                        if(form_error('validDays'))
                            echo "<div class='form-group has-error' id='validDaysDiv'>";
                        else
                            echo "<div class='form-group' id='validDaysDiv'>";
                    ?>
                        <label for="validDays" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_validDays")?>
                        </label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="validDays" name="validDays" value="<?=set_value('validDays')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('validDays'); ?>
                        </span>
                    </div>

					<!--
				   <?php
                    if(form_error('cost'))
                        echo "<div class='form-group has-error' id='costDiv'>";
                    else
                        echo "<div class='form-group' id='costDiv'>";
                    ?>
                        <label for="cost" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_cost")?> <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="cost" name="cost" value="<?=set_value('cost')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('cost'); ?>
                        </span>
                    </div>
					-->
					
                    <?php
                    if(form_error('judge'))
                        echo "<div class='form-group has-error' style='display: none;'>";
                    else
                        echo "<div class='form-group' style='display: none;'>";
                    ?>
                        <label for="judge" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_judge")?>
                        </label>
                        <div class="col-sm-4">
                            <?php
                            $array = [
                                0 => $this->lang->line("online_exam_auto"),
                                1 => $this->lang->line("online_exam_manually")
                            ];
                            echo form_dropdown("judge", $array, set_value("judge"), "id='judge' class='form-control select2'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('judge'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('published'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="published" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_published")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <?php
                                $array['0'] = $this->lang->line("online_exam_select");
                                $array['1'] = $this->lang->line("online_exam_yes");
                                $array['2'] = $this->lang->line("online_exam_no");
                                echo form_dropdown("published", $array, set_value("published"), "id='published' class='form-control select2'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('published'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.select2').select2();

    
    $(document).ready(function() {
        var type = "<?=$posttype?>";
        if(type == 0) {
            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();
            $('#startdateDiv').hide();
            $('#enddateDiv').hide();
        } else if(type == 2) {
            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();
            $('#startdateDiv').hide();
            $('#enddateDiv').hide();
        } else if(type == 4) {
            $('#startdateDiv').show();
            $('#enddateDiv').show();

            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();

            $('#startdate').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY'
            });
            $('#enddate').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY'
            });
        } else if(type == 5) {
            $('#startdatetimeDiv').show();
            $('#enddatetimeDiv').show();

            $('#enddateDiv').hide();
            $('#startdateDiv').hide();

            $('#startdatetime').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY hh:mm a'
            });
            $('#enddatetime').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY hh:mm a'
            });
        } else {
            $('#startdateDiv').hide();
            $('#enddateDiv').hide();
            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();
            $('#validDaysDiv').hide();
            $('#costDiv').hide();
        }        
    });

    $('#type').change(function() {
        var type = $(this).val();
        if(type == 0) {
            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();
            $('#startdateDiv').hide();
            $('#enddateDiv').hide();
        } else if(type == 2) {
            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();
            $('#startdateDiv').hide();
            $('#enddateDiv').hide();
        } else if(type == 4) {
            $('#startdateDiv').show();
            $('#enddateDiv').show();

            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();

            $('#startdate').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY'
            });
            $('#enddate').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY'
            });
        } else if(type == 5) {
            $('#startdatetimeDiv').show();
            $('#enddatetimeDiv').show();

            $('#enddateDiv').hide();
            $('#startdateDiv').hide();

            $('#startdatetime').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY hh:mm a'
            });
            $('#enddatetime').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY hh:mm a'
            });
        }
    });
	
    if($("#jenis").val() == 6){
        $("#jenis_text_field").show();
    } else {
        $("#jenis_text_field").hide();
    }
	
    if($("#jenis").val() == 3 || $("#jenis").val() == 6 || $("#jenis").val() == 5){
        $("#is_show_answer_field").show();
    } else {
        $("#is_show_answer_field").hide();
    }

	$('#jenis').change(function() {
        var jenis = $(this).val();
        if(jenis == 5) {
            $('#instructionss').hide();
        } else if(jenis == 2) {
            $('#instructionss').show();
        } else if(jenis == 3 || jenis == 6) {
           $('#instructionss').show();
        } else if(jenis == 4) {
           $('#instructionss').show();
        }

        if(jenis == 3 || jenis == 6 || jenis == 5) {
            $("#is_show_answer_field").show();
        } else {
            $("#is_show_answer_field").hide();
        }

        if(jenis == 6) {
            $("#jenis_text_field").show();
        } else {
            $("#jenis_text_field").hide();
        }
    });

    $(function() {
        $('#validDaysDiv').hide();
        $('#costDiv').hide();
    })

    $("#classes").change(function() {
        var id = $(this).val();
        
        $.ajax({
            type: 'POST',
            url: "<?=base_url('online_exam/getSection')?>",
            data: {"id" : id},
            dataType: "html",
            success: function(data) {
                $('#section').html(data);
            }
        });

        if(parseInt(id)) {
            if(id === '0') {
                $('#sectionID').val(0);
            } else {
                /**
                * Class is multyple check getSubject
                */
                $.ajax({
                    type: 'POST',
                    url: "<?=base_url('online_exam/getSubject')?>",
                    data: {"classID" : id},
                    dataType: "html",
                    success: function(data) {
                        $('#subject').html(data);
                    }
                });
            }
        }
    });

    $('#ispaid').change(function(event) {
        if($(this).val() == 1) {
            $('#costDiv').show();
        } else {
            $('#costDiv').hide();
        }
    });

    $(document).ready(function() {
        if($('#ispaid').val() == 1) {
            $('#costDiv').show();
        } else {
            $('#costDiv').hide();
        }
    });

</script>
