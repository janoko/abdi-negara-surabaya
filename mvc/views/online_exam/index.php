<?php
//print_r($online_exam);
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-slideshare"></i> <?=$this->lang->line('panel_title')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('panel_title')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <?php
                    if(permissionChecker('online_exam_add')) {
                ?>
                <h5 class="page-header">
                    <a href="<?php echo base_url('online_exam/add') ?>">
                        <i class="fa fa-plus"></i>
                        <?=$this->lang->line('add_title')?>
                    </a>
                </h5>
                <?php } ?>
                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-1"><?=$this->lang->line('slno')?></th>
                                <th class="col-sm-3"><?=$this->lang->line('online_exam_name')?></th>
                                
                                <th class="col-sm-2">Kelas</th>
                                <th class="col-sm-2">Jenis Ujian</th>
								<th class="col-sm-2"><?=$this->lang->line('online_exam_published')?></th>
                                <?php if(permissionChecker('online_exam_edit') || permissionChecker('online_exam_delete') || permissionChecker('online_exam_view')) { ?>
                                    <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(inicompute($online_exams)) {
                                $i = 0; 
                                foreach($online_exams as $online_exam) {
                                    $showStatus = FALSE; 
                                    if($usertypeID == '3') {
                                        if(inicompute($student)) {
                                            $classesId = [];
                                            
                                            foreach($online_exams_classes[$online_exam->onlineExamID] as $oeClass){
                                                $classesId[] = $oeClass->classID;
                                            }
                                            if((count($classesId) == 0 || in_array($student->classesID,$classesId)) 
                                                // && (($online_exam->sectionID == '0') || ($student->sectionID == $online_exam->sectionID)) 
                                                && (($online_exam->studentGroupID == '0') || ($student->studentgroupID == $online_exam->studentGroupID)) 
                                                && ($online_exam->published == '1') 
                                                && (($online_exam->subjectID == '0') || (in_array($online_exam->subjectID, $userSubjectPluck)))
                                            ) {
                                                $showStatus = TRUE;
                                                $i++; 
                                            }
                                        }
                                    } else { 
                                        $i++; 
                                        $showStatus = TRUE;
                                    }

                                    if($showStatus) { ?>
                                    <tr>
                                        <td data-title="<?=$this->lang->line('slno')?>">
                                            <?php echo $i; ?>
                                        </td>
                                        <td data-title="<?=$this->lang->line('online_exam_name')?>">
                                            <?php
                                                if(strlen($online_exam->name) > 40)
                                                    echo strip_tags(substr($online_exam->name, 0, 40)."...");
                                                else
                                                    echo strip_tags(substr($online_exam->name, 0, 40));
                                            ?>
                                        </td>
                                        

                                        <td data-title="Kelas">
                                            <?php
                                            $flag = FALSE;
                                            foreach($online_exams_classes[$online_exam->onlineExamID] as $oeClass){
                                                if(!$flag) $flag = TRUE;
                                                else echo ", ";
                                                echo $oeClass->classes;
                                            }
                                            ?>
                                        </td>
                                        <td data-title="Jenis Ujian">
                                            <?php
                                                if($online_exam->examJenisNumber == 2)
                                                    echo "Pass Hand";
                                                else if($online_exam->examJenisNumber == 3)
                                                    echo "Kecerdasan";
                                                else if($online_exam->examJenisNumber == 4)
                                                    echo "Kepribadian";
                                                else  if($online_exam->examJenisNumber == 5)
                                                    echo "Sikap Kerja";
                                                else  if($online_exam->examJenisNumber == 6)
                                                    echo $online_exam->examJenisText;
                                            
                                            ?>
                                        </td>
                                        
                                        <td data-title="<?=$this->lang->line('online_exam_published')?>">
                                            <?php 
                                                if($online_exam->published == '1') {
                                                    echo "<span class='btn btn-success btn-xs'>Aktif</span>";
                                                } else {
                                                    echo "<span class='btn btn-danger btn-xs'>Belum Aktif</span>";
                                                } 
                                            ?>
                                        </td>
                                        
                                        <?php if(permissionChecker('online_exam_edit') || permissionChecker('online_exam_delete') || permissionChecker('online_exam_view')) { ?>
                                            <td data-title="<?=$this->lang->line('action')?>">
                                                <?php
                                                if($online_exam->examJenisNumber == 5) // jenis exam: sikap kerja
                                                    echo btn_list('online_exam/subexam/'.$online_exam->onlineExamID, 'List Master Ujian', 'online_exam_add');
                                                else
                                                    echo btn_extra('online_exam/addquestion/'.$online_exam->onlineExamID, $this->lang->line('addquestion'), 'online_exam_add');
                                                ?>
                                                <?php  ?>
                                                <?php echo btn_edit('online_exam/edit/'.$online_exam->onlineExamID, $this->lang->line('edit')); ?>
                                                <?php echo btn_copy('online_exam/copydata/'.$online_exam->onlineExamID, "Copy") ?>
                                                <?php // echo btn_copy('online_exam/copydata/'.$question_bank->questionBankID, "Copy") ?>
                                                <?php echo btn_delete('online_exam/delete/'.$online_exam->onlineExamID, $this->lang->line('delete')); ?>
                                            </td>
                                        <?php } ?>
                                    </tr> 
                                    <?php } ?>
                                <?php } ?> 
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>