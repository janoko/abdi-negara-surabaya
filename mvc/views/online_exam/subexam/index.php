<?php
// echo "<pre>";
// print_R($subexams);
// echo "</pre>";
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-slideshare"></i> Sub Exam <?=$parentexam->name; ?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("online_exam/index")?>"> <?=$parentexam->name; ?></a></li>
            <li class="active">Sub Exam</li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <h5 class="page-header">
                    <a href="<?php echo base_url("online_exam/addsubexams/{$parentexam->onlineExamID}") ?>">
                        <i class="fa fa-plus"></i>
                        Tambah Sub Exam
                    </a>
                </h5>

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-2">No</th>
                                <th class="col-sm-2">Judul Sub Exam</th>
                                <th class="col-sm-2">Diterbitkan</th>
                                <th class="col-sm-2">Jenis Ujian</th>
                                <th class="col-sm-2"><?=$this->lang->line('action')?></th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php if(inicompute($subexams)) {$i = 0; foreach($subexams as $subexam) {
                                $showStatus = FALSE; 
                                if($usertypeID == '3') {
                                    if(inicompute($student)) {
                                        $classesId = [];
                                        
                                        foreach($online_exam_classes as $oeClass){
                                            $classesId[] = $oeClass->classID;
                                        }
                                        if((count($classesId) == 0 || in_array($student->classesID,$classesId))
                                            // && ($subexam->sectionID == '0') || (($student->sectionID == $subexam->sectionID)) 
                                            && ($subexam->studentGroupID == '0') || (($student->studentgroupID == $subexam->studentGroupID)) 
                                            && ($subexam->published == '1') 
                                            && (($subexam->subjectID == '0') || (in_array($subexam->subjectID, $userSubjectPluck)))) {
                                            $showStatus = TRUE;
                                            $i++; 
                                        }
                                    }
                                } else { 
                                    $i++; 
                                    $showStatus = TRUE;
                                }

                                if($showStatus) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('subexams_name')?>">
                                        <?php
                                            if(strlen($subexam->name) > 25)
                                                echo strip_tags(substr($subexam->name, 0, 25)."...");
                                            else
                                                echo strip_tags(substr($subexam->name, 0, 25));
                                        ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('subexams_published')?>">
                                        <?php 
                                            if($subexam->published == '1') {
                                                echo "<span class='btn btn-success btn-xs'>Aktif</span>";
                                            } else {
                                                echo "<span class='btn btn-danger btn-xs'>Belum Aktif</span>";
                                            } 
                                        ?>
                                    </td>

                                    <td data-title="Jenis Ujian">
										<?php
											if($subexam->examJenisNumber == 2)
												echo "Pass Hand";
											else if($subexam->examJenisNumber == 3)
												echo "Kecerdasan";
											else if($subexam->examJenisNumber == 3)
												echo "Kepribadian";
											else  if($subexam->examJenisNumber == 5)
												echo "Sikap Kerja";
                                            else  if($online_exam->examJenisNumber == 6)
                                                echo $online_exam->examJenisText;
										
										?>
                                    </td>

                                        <td data-title="<?=$this->lang->line('action')?>">
                                            <?php echo btn_extra('online_exam/addquestion/'.$subexam->onlineExamID, $this->lang->line('addquestion'), 'online_exam_add'); ?>
                                            <?php echo btn_editsub('online_exam/editsubexam/'.$subexam->onlineExamID, $this->lang->line('edit')); ?>
                                            <?php echo btn_delete('online_exam/delete/'.$subexam->onlineExamID, $this->lang->line('delete')); ?>
                                        </td>

                                </tr> 
                            <?php } } } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>