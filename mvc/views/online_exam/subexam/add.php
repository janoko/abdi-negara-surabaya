
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-slideshare"></i> <?=$this->lang->line('panel_title')?></h3>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("online_exam/index")?>"><?=$this->lang->line('menu_online_exam')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_online_exam')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form" method="post">
                    <?php
                    if(form_error('name'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="name" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_name")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name')?>">
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('name'); ?>
                        </span>
                    </div>
					
					
                    <?php
                    if(form_error('instruction'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="instruction" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_instruction")?>
                        </label>
                        <div class="col-sm-6">
                            <?php
                            $arrayInstruction = array(0 => $this->lang->line("online_exam_select"));
                            if(inicompute($instructions)) {
                                foreach ($instructions as $instruction) {
                                    $arrayInstruction[$instruction->instructionID] = $instruction->title;
                                }
                            }
                            echo form_dropdown("instruction", $arrayInstruction, set_value("instruction"), "id='instruction' class='form-control select2'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('instruction'); ?>
                        </span>
                    </div>

                    <?php
                    if(form_error('published'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                        <label for="published" class="col-sm-2 control-label">
                            <?=$this->lang->line("online_exam_published")?>
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-6">
                            <?php
                                $array['0'] = $this->lang->line("online_exam_select");
                                $array['1'] = $this->lang->line("online_exam_yes");
                                $array['2'] = $this->lang->line("online_exam_no");
                                echo form_dropdown("published", $array, set_value("published"), "id='published' class='form-control select2'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('published'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.select2').select2();

    
    $(document).ready(function() {
        var type = "<?=$posttype?>";
        if(type == 0) {
            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();
            $('#startdateDiv').hide();
            $('#enddateDiv').hide();
        } else if(type == 2) {
            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();
            $('#startdateDiv').hide();
            $('#enddateDiv').hide();
        } else if(type == 4) {
            $('#startdateDiv').show();
            $('#enddateDiv').show();

            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();

            $('#startdate').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY'
            });
            $('#enddate').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY'
            });
        } else if(type == 5) {
            $('#startdatetimeDiv').show();
            $('#enddatetimeDiv').show();

            $('#enddateDiv').hide();
            $('#startdateDiv').hide();

            $('#startdatetime').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY hh:mm a'
            });
            $('#enddatetime').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY hh:mm a'
            });
        } else {
            $('#startdateDiv').hide();
            $('#enddateDiv').hide();
            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();
            $('#validDaysDiv').hide();
            $('#costDiv').hide();
        }        
    });

    $('#type').change(function() {
        var type = $(this).val();
        if(type == 0) {
            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();
            $('#startdateDiv').hide();
            $('#enddateDiv').hide();
        } else if(type == 2) {
            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();
            $('#startdateDiv').hide();
            $('#enddateDiv').hide();
        } else if(type == 4) {
            $('#startdateDiv').show();
            $('#enddateDiv').show();

            $('#startdatetimeDiv').hide();
            $('#enddatetimeDiv').hide();

            $('#startdate').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY'
            });
            $('#enddate').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY'
            });
        } else if(type == 5) {
            $('#startdatetimeDiv').show();
            $('#enddatetimeDiv').show();

            $('#enddateDiv').hide();
            $('#startdateDiv').hide();

            $('#startdatetime').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY hh:mm a'
            });
            $('#enddatetime').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY hh:mm a'
            });
        }
    });

    $(function() {
        $('#validDaysDiv').hide();
        $('#costDiv').hide();
    })

    $("#classes").change(function() {
        var id = $(this).val();
        if(parseInt(id)) {
            if(id === '0') {
                $('#sectionID').val(0);
            } else {
                $.ajax({
                    type: 'POST',
                    url: "<?=base_url('online_exam/getSection')?>",
                    data: {"id" : id},
                    dataType: "html",
                    success: function(data) {
                        $('#section').html(data);
                    }
                });
                
                $.ajax({
                    type: 'POST',
                    url: "<?=base_url('online_exam/getSubject')?>",
                    data: {"classID" : id},
                    dataType: "html",
                    success: function(data) {
                        $('#subject').html(data);
                    }
                });
            }
        }
    });

    $('#ispaid').change(function(event) {
        if($(this).val() == 1) {
            $('#costDiv').show();
        } else {
            $('#costDiv').hide();
        }
    });

    $(document).ready(function() {
        if($('#ispaid').val() == 1) {
            $('#costDiv').show();
        } else {
            $('#costDiv').hide();
        }
    });

</script>
