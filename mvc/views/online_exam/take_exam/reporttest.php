<style>
.question-answer {
    margin-bottom: 2%;
    margin-top: 0%;
    width: 100%;
    height: auto;
}
.fuelux .wizard .step-content {
	border: 0px;
}
td { font-size: 17px; padding: 5px; font-weight: 500;}
label {
	display: inline-block;
	margin-bottom: 5px;
	font-weight: 14;
	font-size: 17px;
}
.question-answer .table > thead > tr > th, .question-answer .table > tfoot > tr > th, .question-answer .table > tbody > tr > td {
    line-height: 20px;
}
</style>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"><i class="fa fa-user-secret"></i> Laporan Jawaban Ujian <?=xGetOne("select name from online_exam where onlineExamID = {$onlineExamID}")?></h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<div class="box-body">
					<div class="row">
						<div class="col-sm-12">
						
                <div class="col-sm-12">
                    <?php 
                        if(inicompute($questions)) {
                        $i = 0;
                        foreach($questions as $question) {
                            $optionCount = $question->totalOption;
                            $i++; ?>
                            <div class="clearfix">
                                <div class="question-body">
									<br>
									<br>
                                   
									<?php
									if($onlineExam->examJenisNumber == 5) // jenis exam: sikap kerja
                                    { ?>
									 <label><b><?=$i?>.</b></label>
									 <?php 
										$instructionQuest = getInstruction($question->instructionID);
										echo $instructionQuest->content;
										echo "<br>";
									?>
									<label> <?=strip_tags($question->question)?></label>
									<?php } else { ?>
									<label><b><?=$i?>.</b> <?=strip_tags($question->question)?></label>
									<?php } ?>
                                </div>

                                <?php if($question->upload != '') { ?>
                                    <div>
                                        <img style="width:250px;height:150px;padding-left: 20px" src="<?=base_url('uploads/images/'.$question->upload)?>" alt="">
                                    </div>
                                <?php } ?>

                                <div class="question-answer">
                                    <table class="table">
                                        <tr>
                                        <?php
                                            $oc = 1;
                                            $tdCount = 0;
                                            $questionoptions = isset($question_options[$question->questionBankID]) ? $question_options[$question->questionBankID] : [];
                                            if(inicompute($questionoptions)) {
                                                $optionLabel = 'A';
                                                foreach ($questionoptions as $option) {
                                                    if($optionCount >= $oc) { $oc++;
                                                        if(isset($examquestionsuseranswer[$question->questionBankID]) && $option->optionID == $examquestionsuseranswer[$question->questionBankID]->optionID) {
                                                            if(isset($examquestionsanswer[$question->questionBankID]) && $examquestionsanswer[$question->questionBankID]->optionID==$examquestionsuseranswer[$question->questionBankID]->optionID) {
                                                                ?>
                                                                <td style="background: green;">
                                                                    <span style="color: #ffffff"><?=strip_tags($optionLabel)?>.</span>
                                                                    <span style="color: #ffffff"><?=strip_tags($option->name)?></span>
                                                                    <label for="option<?= $option->optionID ?>">
                                                                        <?php
                                                                        if (!is_null($option->img) && $option->img != "") { ?>
                                                                            <img class="questionimg"
                                                                                 src="<?= base_url('uploads/images/' . $option->img) ?>"/>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </label>
                                                                </td>
                                                                <?php
                                                                $optionLabel++;
                                                            }else { ?>
                                                                <td style="background: red;">
                                                                    <span
                                                                        style="color: #ffffff"><?=strip_tags($optionLabel)?>.</span>
                                                                    <span
                                                                        style="color: #ffffff"><?=strip_tags($option->name)?></span>
                                                                    <label for="option<?= $option->optionID ?>">
                                                                        <?php
                                                                        if (!is_null($option->img) && $option->img != "") { ?>
                                                                            <img class="questionimg"
                                                                                 src="<?= base_url('uploads/images/' . $option->img) ?>"/>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </label>
                                                                </td>

                                                                <?php

                                                                $optionLabel++;
                                                            }
                                                    } else {
                                                            if (isset($examquestionsanswer[$question->questionBankID]) && $option->optionID == $examquestionsanswer[$question->questionBankID]->optionID) { ?>
                                                                <td>
                                                                    <span><?=$optionLabel?>.</span>
                                                                    <span> <?=strip_tags($option->name)?></span>
                                                                    <span class="selected_div"><i class="fa fa-check fa-2x text-blue"></i></span>
                                                                    <label for="option<?=$option->optionID?>">
                                                                        <?php
                                                                        if(!is_null($option->img) && $option->img != "") { ?>
                                                                            <img class="questionimg" src="<?=base_url('uploads/images/'.$option->img)?>"/>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </label>
                                                                </td>
                                                                <?php
                                                                $optionLabel++;
                                                            }else {?>

                                                               <td>
                                                                <span><?=$optionLabel?>.</span>
                                                                <span><?=strip_tags($option->name)?></span>
                                                                <label for="option<?=$option->optionID?>">
                                                                    <?php
                                                                    if(!is_null($option->img) && $option->img != "") { ?>
                                                                        <img class="questionimg" src="<?=base_url('uploads/images/'.$option->img)?>"/>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </label>
                                                                </td>
                                                                <?php
                                                                $optionLabel++;
                                                            }
                                                        }
                                                    }
                                                    $tdCount++;
                                                    if($tdCount == 6) {
                                                        $tdCount = 0;
                                                        echo "</tr><tr>";
                                                    }
                                                }
                                            }
                                        ?>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                    <?php } } else { ?>
                        <div class="callout callout-danger">
                            <p><b class="text-info"><?=$this->lang->line('onlineexamquestionanswerreport_data_not_found')?></b></p>
                        </div>
                    <?php } ?>
                </div>
										
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

        