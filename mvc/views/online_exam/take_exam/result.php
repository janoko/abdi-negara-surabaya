<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style>
.highcharts-figure, .highcharts-data-table table {
    min-width: 360px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>

<section class="panel">
    <div class="panel-body">
        <div id="printablediv">
            <div class="row">

                <div class="col-sm-3">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <?=profileviewimage($student->photo)?>
                            <h3 class="profile-username text-center"><?=$student->name?></h3>
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item" style="background-color: #FFF">
                                    <b><?=$this->lang->line('take_exam_registerNO')?></b> <a class="pull-right"><?=$student->registerNO?></a>
                                </li>
                                <li class="list-group-item" style="background-color: #FFF">
                                    <b><?=$this->lang->line('take_exam_class')?></b> <a class="pull-right"><?=inicompute($class) ? $class->classes : ''?></a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#profile" data-toggle="tab"><?=$this->lang->line('take_exam_exam_info')?> <?php echo $dataujian['name']; ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="profile">
                                <div class="panel-body profile-view-dis">
                                    <div class="row" style="min-height: 250px">
									
										<?php if($examparent['examJenisNumber'] != 5) { ?>
                                        <h2>
                                            <?php 
                                                if(inicompute($onlineExam)) {
                                                    if($onlineExam->markType == 5) {
                                                        $percentage = 0;

														if($online_exam_user_status['totalObtainedMark'] > 0 && $online_exam_user_status['totalMark'] > 0) {
                                                           // $percentage = (($online_exam_user_status['totalObtainedMark'] /$online_exam_user_status['totalMark'] )*100);
															$percentage = $online_exam_user_status['totalPercentage'];
															if($percentage < 50)
																$predikat = "Tidak Lulus";
															else if($percentage >= 50 && $percentage <= 59)
																$predikat = "Kurang";
															else if($percentage >= 60 && $percentage <= 69)
																$predikat = "Cukup";
															else if($percentage >= 70 && $percentage <= 79)
																$predikat = "Baik";
															else if($percentage >= 80 && $percentage <= 100)
																$predikat = "Lulus";
															else
																$predikat = "Sangat Kurang";
															
                                                        } 

                                                        if($percentage >= 71) {
															echo '<span class="text-green">'.$predikat.'</span>';
														} else {
															echo '<span class="text-red">'.$predikat.'</span>';
														}
														
                                                    } elseif($onlineExam->markType == 10) {
														
														if($online_exam_user_status['totalObtainedMark'] > 0 && $online_exam_user_status['totalMark'] > 0) {
                                                            //$percentage = (($online_exam_user_status['totalObtainedMark'] /$online_exam_user_status['totalMark'] )*100);
                                                            $percentage = $online_exam_user_status['totalPercentage'];
															if($percentage < 50)
																$predikat = "Tidak Lulus";
															else if($percentage >= 50 && $percentage <= 59)
																$predikat = "Kurang";
															else if($percentage >= 60 && $percentage <= 69)
																$predikat = "Cukup";
															else if($percentage >= 70 && $percentage <= 79)
																$predikat = "Baik";
															else if($percentage >= 80 && $percentage <= 100)
																$predikat = "Lulus";
															else
																$predikat = "Sangat Kurang";
															
															
															if($percentage >= 71) {
																echo '<span class="text-green">'.$predikat.'</span>';
															} else {
																echo '<span class="text-red">'.$predikat.'</span>';
															}
                                                        } 
                                                    }
                                                } 
                                            ?>
                                        </h2>
                                        <table class="table table-bordered">
                                            <tr>
                                                <td><?=$this->lang->line('take_exam_total_question')?> : <?=inicompute($onlineExamQuestions)?></td>
                                                <td>Total Pertanyaan Dijawab : <?=$online_exam_user_status['totalAnswer']?></td>
                                            </tr>
                                            <tr>
                                                <td><?=$this->lang->line('take_exam_total_current_answer')?> : <?=$online_exam_user_status['totalCurrectAnswer']?>
                                                <td><?=$this->lang->line('take_exam_total_mark')?> : <?=$online_exam_user_status['totalMark']?></td>
                                            </tr> 
                                            <tr>
                                                <td><?=$this->lang->line('take_exam_total_obtained_mark')?> : <?=$online_exam_user_status['totalObtainedMark']?></td>
                                                <td>Total Nilai Akhir : <?=$online_exam_user_status['totalPercentage']?></td>
                                            </tr>
                                        </table>

										<?php // jenis exam: kecerdasan ?>
										<?php if(($onlineExam->examJenisNumber == 3 || $onlineExam->examJenisNumber == 6) && $onlineExam->is_show_answer == 1){ ?>
											<button id="get_question_list" class="btn btn-success" onclick="newPopup('<?=base_url("take_exam/reporttest/{$online_exam_user_status['onlineExamID']}/{$online_exam_user_status['userID']}/{$online_exam_user_status['examtimeID']}")?>')" style="margin-top:23px;">Jawaban</a></button>
										<?php } ?>

										<?php // jenis exam: passhand, kepribadian ?>
										<?php if($onlineExam->examJenisNumber == 4 || $onlineExam->examJenisNumber == 2){ ?>
											<h2>Keterangan Hasil Ujian :</h2>
											
											<b><p>Total Nilai Akhir = (Total Score Diraih / Total Score Penuh) x 100</p>
											<b><p>Total Score Penuh = 4 x Jumlah Soal</p>
											<b><p>Nilai lebih dari 81, Predikat Lulus</p>
											<p>Nilai antara 70 sd 80, Predikat Baik</p>
											<p>Nilai antara 61 sd 70, Predikat Cukup</p>
											<p>Nilai antara 50 sd 60, Predikat Kurang</p>
											<p>Nilai kurang dari 50, Predikat Tidak Lulus</p>
										<?php } ?>
										
									<?php } ?>
									
									
									<?php 
									// jenis exam: sikap kerja
									if($examparent['examJenisNumber'] == 5 && $examparent['jenisExam'] == 'parent') { ?>
										<h2>Keterangan Hasil Ujian :</h2>
										<b><p>Prosentase > 70 % = Lulus</p>
										<p>Prosentase < 50% = Predikat Kurang</p>
										<p>Prosentase lebih dari 50% kurang dari sama dengan 70 = Predikat Cukup</p>
										<p>Prosentase lebih dari 70% kurang dari sama dengan 85 = Predikat Baik</p>
										<p>Prosentase lebih dari 85% = Predikat Sangat Baik</p></b>
										<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
											<thead>
												<tr>
													<th class="col-sm-1">A</th>
													<th class="col-sm-2">B</th>
													<th class="col-sm-1">C</th>
													<th class="col-sm-1">D</th>
													<th class="col-sm-1">E</th>
													<th class="col-sm-1">F</th>
													<th class="col-sm-1">G</th>
													<th class="col-sm-1">H</th>
													<th class="col-sm-1">I</th>
												</tr>
												<tr>
													<th class="col-sm-1"></th>
													<th class="col-sm-1"></th>
													<th class="col-sm-1"></th>
													<th class="col-sm-1"></th>
													<th class="col-sm-1"></th>
													<th class="col-sm-1"></th>
													<th class="col-sm-1"></th>
													<th class="col-sm-1"></th>
													<th class="col-sm-1">G - F -I</th>
												</tr>
												<tr>
													<th class="col-sm-1">#</th>
													<th class="col-sm-2">Master</th>
													<th class="col-sm-1">B</th>
													<th class="col-sm-1">S</th>
													<th class="col-sm-1">Nilai Benar</th>
													<th class="col-sm-1">Pinalty Kesalahan per Kolom</th>
													<th class="col-sm-1">Score</th>
													<th class="col-sm-1">Grafik Turun</th>
													<th class="col-sm-1">Nilai Sikap Kerja</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$totalQuest = 0;
												$totalAns = 0;
												$totalCur = 0;
												$totalMar = 0;
												$totalObt = 0;
												$totalPerc = 0;
												$totalPercCorrectAnswer = 0;
												$totalScoreCorrectAnswer = 0;
												$totalNegativePoint = 0;
												$totalScoreTurnDown = 0;
												
												$prev_correct_answers = 0;
												$num_of_turn_down = 0;

												$i=0; 
												foreach($userExamCheck as $exams){
													$i++;
													$num_of_questions 		= (int)$exams['totalQuestion'];
													$num_of_answers 		= (int)$exams['totalAnswer'];
													$num_of_correct_answers	= (int)$exams['totalCurrectAnswer'];
													$num_of_mark 			= (int)$exams['totalMark'];
													$num_of_obtained_mark 	= (int)$exams['totalObtainedMark'];
													$scr_of_correct_answers = $num_of_correct_answers * 2;
													$num_of_wrong_answers	= $num_of_answers - $num_of_correct_answers;
													$percentage_of_point 	= 100.0*$num_of_correct_answers / ($num_of_questions);
													$negative_point			= 0;
													$graph_down				= 0;

													// negative point
													if ($num_of_wrong_answers >= 1 && $num_of_wrong_answers <= 2)
														$negative_point = 0.5;
													else if ($num_of_wrong_answers >= 3 && $num_of_wrong_answers <= 5)
														$negative_point = 1.0;
													else if ($num_of_wrong_answers >= 5 && $num_of_wrong_answers <= 10)
														$negative_point = 1.5;
													else if ($num_of_wrong_answers > 10)
														$negative_point = 2.0;
													
													if ($num_of_correct_answers < $prev_correct_answers) {
														$graph_down = 1;
														$num_of_turn_down += 1;
													}

													$prev_correct_answers = $num_of_correct_answers;

													$totalQuest += $num_of_questions;
													$totalAns += $num_of_answers;
													$totalCur += $num_of_correct_answers;
													$totalMar += $num_of_mark;
													$totalObt += $num_of_obtained_mark;
													$totalPercCorrectAnswer += $percentage_of_point;
													$totalScoreCorrectAnswer += $scr_of_correct_answers;
													$totalNegativePoint += $negative_point;

													?>	
													<tr row="0">
														<td data-title="No.">
															<?php echo $i; ?>
														</td>
														<td data-title="Master">
															<?php echo xGetOne("select name from online_exam where onlineExamID = {$exams['onlineExamID']}"); ?>
														</td>
														<td data-title="jawaban-benar">
															<?php echo $num_of_correct_answers; ?>
														</td>
														<td data-title="jawaban-salah">
															<?php echo($num_of_answers - $num_of_correct_answers); ?>
														</td>
														<td data-title="score-jawaban-benar">
															<?php echo $scr_of_correct_answers; ?>
														</td>
														<td data-title="pinalty-kesalahan-per-kolom">
															<?php echo $negative_point; ?>
														</td>
														<td data-title="score">
															<?php echo number_format($percentage_of_point, 2, '.', ''); ?>
														</td>
														<td data-title="graph-down">
															<?php if ($graph_down) echo $graph_down; ?>
														</td>
														<td data-title="Laporan">
															<?php if($onlineExam->is_show_answer == 1){ ?>
																<button id="get_question_list" class="btn btn-success" onclick="newPopup('<?=base_url("take_exam/reporttest/{$exams['onlineExamID']}/{$exams['userID']}/{$exams['examtimeID']}")?>')" style="margin-top:2px;">Jawaban</a></button>
															<?php } ?>
														</td>
													</tr>
												<?php } 
												
												// negative point for turn down graph
												$negative_point_desc = $totalNegativePoint." + ";
												if ($num_of_turn_down == 1) {
													$totalScoreTurnDown = 1;
													$negative_point_desc .= 1;
												}
												else if ($num_of_turn_down == 2) {
													$totalScoreTurnDown = 2;
													$negative_point_desc .= 2;
												}
												else if ($num_of_turn_down == 3) {
													$totalScoreTurnDown = 3;
													$negative_point_desc .= 3;
												}
												else if ($num_of_turn_down == 4) {
													$totalScoreTurnDown = 4;
													$negative_point_desc .= 4;
												}
												else if ($num_of_turn_down == 5) {
													$totalScoreTurnDown = 5;
													$negative_point_desc .= 5;
												}
												else if ($num_of_turn_down > 5) {
													$totalScoreTurnDown = 10;
													$negative_point_desc .= 10;
												}
												$negative_point_desc .= " (grafik turun ".$num_of_turn_down."x)";

												if ($i > 0) $totalPercCorrectAnswer /= $i;
												$totalPerc = $totalPercCorrectAnswer - $totalNegativePoint - $totalScoreTurnDown;
												?>
												
												<tr row="1">
													<td data-title="No.">
														-
													</td>
													<td data-title="Master">
														<b>Nilai Total</b>
													</td>
													<td data-title="jawaban-benar"></td>
													<td data-title="jawaban-salah"></td>
													<td data-title="score-jawaban-benar"></td>
													<td data-title="pinalty-kesalahan-per-kolom">
														<h5><?php echo $totalNegativePoint; ?></h5>
													</td>
													<td data-title="score">
														<h5><?php echo number_format($totalPercCorrectAnswer, 2, '.', ''); ?></h5>
													</td>
													<td data-title="graph-down">
														<h5><?php echo $totalScoreTurnDown; ?></h5>
													</td>
													
													<td data-title="Laporan">
														<?php 
															echo "<h4>".number_format($totalPerc, 2, '.', '')."</h4>";
															if($totalPerc <= 50)
																$predikat = "Kurang";
															else if($totalPerc >= 51 && $totalPerc < 70)
																$predikat = "Cukup";
															else if($totalPerc >= 70 && $totalPerc < 85)
																$predikat = "Baik";
															else if($totalPerc >= 85 && $totalPerc <= 100)
																$predikat = "Sangat Baik";
															else
																$predikat = "Sangat Kurang";
															
															if(($totalPerc / $i) >= 70) {
																echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . ' dengan Predikat '.$predikat.'</span>';
															} else {
																echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . ' dengan Predikat '.$predikat.'</span>';
															}
														?>
													</td>
												</tr>
											</tbody>
										</table>
										
										
				
									<?php } else { ?>
										
                                        <?php 
										// echo "<pre>";
										// print_r($userExamCheck);
										// echo "</pre>";
										
										//if(inicompute($userExamCheck)) { foreach($userExamCheck as $examCheck) { ?>
										<!--                                          
										  <br>
											<h3>Percobaan ke <?php echo $examCheck['examtimeID']; ?> : 
                                            <?php 
                                                // if(inicompute($onlineExam)) {
													
                                                    // if($onlineExam->markType == 5) {
														// if($examCheck['totalObtainedMark'] > 0 && $examCheck['totalMark'] > 0) {
                                                            // $percentage = (($examCheck['totalObtainedMark'] /$examCheck['totalMark'] )*100);
															// if($percentage < 50)
																// $predikat = "Kurang";
															// else if($percentage > 50 && $percentage < 65)
																// $predikat = "Cukup";
															// else if($percentage > 65 && $percentage < 80)
																// $predikat = "Baik";
															// else if($percentage > 80 && $percentage < 100)
																// $predikat = "Sangat Baik";
															// else
																// $predikat = "Sangat Kurang";
															
															
															 // // if($examCheck->totalPercentage >= $onlineExam->percentage) {
																// // echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . ' dengan Predikat '.$predikat.'</span>';
															// // } else {
																// // echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . ' dengan Predikat '.$predikat.'</span>';
															// // }
                                                        // } 
														
                                                        // // $percentage = 0;
                                                        // // if($examCheck->totalObtainedMark > 0 && $examCheck->totalMark > 0) {
                                                            // // $percentage = (($examCheck->totalObtainedMark/$examCheck->totalMark)*100);
                                                        // // } 

                                                        // if($percentage >= $onlineExam->percentage) {
                                                           // echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . ' dengan Predikat '.$predikat.'</span>';
                                                        // } else {
                                                           // echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . ' dengan Predikat '.$predikat.'</span>';
                                                        // }
                                                    // } elseif($onlineExam->markType == 10) {
														// if($examCheck['totalObtainedMark'] > 0 && $examCheck['totalMark'] > 0) {
                                                            // $percentage = (($examCheck['totalObtainedMark'] /$examCheck['totalMark'] )*100);
															// if($percentage < 50)
																// $predikat = "Kurang";
															// else if($percentage > 50 && $percentage < 65)
																// $predikat = "Cukup";
															// else if($percentage > 65 && $percentage < 80)
																// $predikat = "Baik";
															// else if($percentage > 80 && $percentage < 100)
																// $predikat = "Sangat Baik";
															// else
																// $predikat = "Sangat Kurang";
															
															
															 // // if($examCheck->totalPercentage >= $onlineExam->percentage) {
																// // echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . ' dengan Predikat '.$predikat.'</span>';
															// // } else {
																// // echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . ' dengan Predikat '.$predikat.'</span>';
															// // }
                                                        // }
														
                                                        // //if($examCheck['totalObtainedMark'] >= $onlineExam->percentage) {
                                                        // if($examCheck['totalPercentage'] >= $onlineExam->percentage) {
                                                            // echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . ' dengan Predikat '.$predikat.'</span>';
                                                        // } else {
                                                            // echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . ' dengan Predikat '.$predikat.'</span>';
                                                        // }
                                                    // }
                                                // } 
                                            ?>
                                            </h3>
											
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td><?=$this->lang->line('take_exam_total_question')?> : <?=$examCheck['totalQuestion']?></td>
                                                    <td><?=$this->lang->line('take_exam_total_answer')?> : <?=$examCheck['totalAnswer']?></td>
                                                </tr>
                                                <tr>
                                                    <td><?=$this->lang->line('take_exam_total_current_answer')?> : <?=$examCheck['totalCurrectAnswer']?></td>
                                                    <td><?=$this->lang->line('take_exam_total_mark')?> : <?=$examCheck['totalMark']?></td>
                                                </tr> 
                                                <tr>
                                                    <td><?=$this->lang->line('take_exam_total_obtained_mark')?> : <?=$examCheck['totalObtainedMark']?></td>
                                                    <td><?=$this->lang->line('take_exam_total_percentage')?> : <?=number_format($examCheck['totalPercentage'],2)?> % </td>
                                                </tr>
                                            </table>
                                        <?php // }  ?>
										-->
									<?php } // } ?>
                                    </div>
									<div class="col-sm-12 col-md-12 col-lg-12" id="containerNilai"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="application/javascript">
    function newPopup(url) {
        var myWindowStatus = false;
        
		myWindowStatus = true;
		myWindow = window.open(url,'_blank',"width=1000,height=650,toolbar=0,location=0,scrollbars=yes");
		runner();
	}
</script>

<script type="text/javascript" >
    $('.sidebar-menu li a').css('pointer-events', 'none');

    function disableF5(e) {
        if ( ( (e.which || e.keyCode) == 116 ) || ( e.keyCode == 82 && e.ctrlKey ) ) {
            e.preventDefault();
        }
    }

    $(document).bind("keydown", disableF5);

    // function Disable(event) {
        // if (event.button == 2)
        // {
            // window.oncontextmenu = function () {
                // return false;
            // }
        // }
    // }
    // document.onmousedown = Disable;
</script>


<script>
<?php // jenis exam: sikap kerja ?>
<?php if($examparent['examJenisNumber'] == 5 && $examparent['jenisExam'] == 'parent') { ?>
	Highcharts.chart('containerNilai', {
		
		title: {
			text: 'Grafik Nilai'
		},

		subtitle: {
			text: ''
		},

		yAxis: {
			title: {
				text: 'Nilai'
			}
		},

		xAxis: {
			title: {
				text: 'Master Soal'
			},
			categories: <?= json_encode($arrChart['master']); ?>
		},

		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle'
		},


		series: [{
			name: 'Score',
			data: <?= json_encode($arrChart['totalObtainedMark']); ?>
		}, {
			name: 'Total Jawaban Benar',
			data: <?= json_encode($arrChart['totalCurrectAnswer']); ?>
		}],

		responsive: {
			rules: [{
				condition: {
					maxWidth: 400
				},
				chartOptions: {
					legend: {
						layout: 'horizontal',
						align: 'center',
						verticalAlign: 'bottom'
					}
				}
			}]
		}

	});
<?php } ?>
$('#example1').dataTable( {
  "pageLength": 20
} );

 $("#get_question_list").click(function() {
        var error = 0 ;
        var field ={
            'onlineExamID' : 13,
            'studentID' : 2,
            'attemptID' : 1,
        }

        if (field['onlineExamID'] == 0) {
            $('#examDiv').addClass('has-error');
            error++;
        } else {
            $('#examDiv').removeClass('has-error');
        }


        if (field['studentID'] == 0) {
            $('#studentDiv').addClass('has-error');
            error++;
        } else {
            $('#studentDiv').removeClass('has-error');
        }
        if (field['attemptID'] == 0) {
            $('#attemptDiv').addClass('has-error');
            error++;
        } else {
            $('#attemptDiv').removeClass('has-error');
        }

        if(error === 0) {
            makingPostDataPreviousofAjaxCall(field);
        }
    });

    function makingPostDataPreviousofAjaxCall(field) {
        passData = field;
        ajaxCall(passData);
    }

    function ajaxCall(passData) {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('take_exam/getQuestionList')?>",
            data: passData,
            dataType: "html",
            success: function(data) {
                var response = JSON.parse(data);
                renderLoder(response, passData);
            }
        });
    }

    function renderLoder(response, passData) {
        if(response.status) {
            $('#load_take_exam').html(response.render);
            for (var key in passData) {
                if (passData.hasOwnProperty(key)) {
                    $('#'+key).parent().removeClass('has-error');
                }
            }
        } else {
            for (var key in passData) {
                if (passData.hasOwnProperty(key)) {
                    $('#'+key).parent().removeClass('has-error');
                }
            }

            for (var key in response) {
                if (response.hasOwnProperty(key)) {
                    $('#'+key).parent().addClass('has-error');
                }
            }
        }
    }
</script>