<div class="box">
    <div class="box-header bg-gray">
        <h3 class="box-title text-navy"><i class="fa fa-clipboard"></i> 
        <?=$this->lang->line('onlineexamreport_report_for')?> - <?=$this->lang->line('onlineexamreport_onlineexam')?></h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div id="printablediv">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12" style="margin-bottom: 25px;">
                    <?=reportheader($siteinfos)?>
                </div>
                <div class="col-sm-12">
                   <div class="box box-solid " style="border: 1px #ccc solid; border-left: 2px black solid">
                        <div class="box-header bg-gray with-border">
                            <h3 class="box-title text-navy"><?=$this->lang->line("onlineexamreport_examinformation")?></h3>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-info fa-2x"></i></li>
                            </ol>
                        </div>
                        <div class="box-body">
                            <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-sm-1">A</th>
                                        <th class="col-sm-2">B</th>
                                        <th class="col-sm-1">C</th>
                                        <th class="col-sm-1">D</th>
                                        <th class="col-sm-1">E</th>
                                        <th class="col-sm-1">F</th>
                                        <th class="col-sm-1">G</th>
                                        <th class="col-sm-1">H</th>
                                        <th class="col-sm-1">I</th>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-1"></th>
                                        <th class="col-sm-1"></th>
                                        <th class="col-sm-1"></th>
                                        <th class="col-sm-1"></th>
                                        <th class="col-sm-1"></th>
                                        <th class="col-sm-1"></th>
                                        <th class="col-sm-1"></th>
                                        <th class="col-sm-1"></th>
                                        <th class="col-sm-1">G - F -I</th>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-1">#</th>
                                        <th class="col-sm-2">Master</th>
                                        <th class="col-sm-1">B</th>
                                        <th class="col-sm-1">S</th>
                                        <th class="col-sm-1">Nilai Benar</th>
                                        <th class="col-sm-1">Pinalty Kesalahan per Kolom</th>
                                        <th class="col-sm-1">Score</th>
                                        <th class="col-sm-1">Grafik Turun</th>
                                        <th class="col-sm-1">Nilai Sikap Kerja</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $totalQuest = 0;
                                    $totalAns = 0;
                                    $totalCur = 0;
                                    $totalMar = 0;
                                    $totalObt = 0;
                                    $totalPerc = 0;
                                    $totalPercCorrectAnswer = 0;
                                    $totalScoreCorrectAnswer = 0;
                                    $totalNegativePoint = 0;
                                    $totalScoreTurnDown = 0;
                                    
                                    $prev_correct_answers = 0;
                                    $num_of_turn_down = 0;

                                    $i=0; 
                                    foreach($userExamCheck as $exams){
                                        $i++;
                                        $num_of_questions 		= (int)$exams['totalQuestion'];
                                        $num_of_answers 		= (int)$exams['totalAnswer'];
                                        $num_of_correct_answers	= (int)$exams['totalCurrectAnswer'];
                                        $num_of_mark 			= (int)$exams['totalMark'];
                                        $num_of_obtained_mark 	= (int)$exams['totalObtainedMark'];
                                        $scr_of_correct_answers = $num_of_correct_answers * 2;
                                        $num_of_wrong_answers	= $num_of_answers - $num_of_correct_answers;
                                        $percentage_of_point 	= 100.0*$num_of_correct_answers / ($num_of_questions);
                                        $negative_point			= 0;
                                        $graph_down				= 0;

                                        // negative point
                                        if ($num_of_wrong_answers >= 1 && $num_of_wrong_answers <= 2)
                                            $negative_point = 0.5;
                                        else if ($num_of_wrong_answers >= 3 && $num_of_wrong_answers <= 5)
                                            $negative_point = 1.0;
                                        else if ($num_of_wrong_answers >= 5 && $num_of_wrong_answers <= 10)
                                            $negative_point = 1.5;
                                        else if ($num_of_wrong_answers > 10)
                                            $negative_point = 2.0;
                                        
                                        if ($num_of_correct_answers < $prev_correct_answers) {
                                            $graph_down = 1;
                                            $num_of_turn_down += 1;
                                        }

                                        $prev_correct_answers = $num_of_correct_answers;

                                        $totalQuest += $num_of_questions;
                                        $totalAns += $num_of_answers;
                                        $totalCur += $num_of_correct_answers;
                                        $totalMar += $num_of_mark;
                                        $totalObt += $num_of_obtained_mark;
                                        $totalPercCorrectAnswer += $percentage_of_point;
                                        $totalScoreCorrectAnswer += $scr_of_correct_answers;
                                        $totalNegativePoint += $negative_point;

                                        ?>	
                                        <tr row="0">
                                            <td data-title="No.">
                                                <?php echo $i; ?>
                                            </td>
                                            <td data-title="Master">
                                                <?php echo xGetOne("select name from online_exam where onlineExamID = {$exams['onlineExamID']}"); ?>
                                            </td>
                                            <td data-title="jawaban-benar">
                                                <?php echo $num_of_correct_answers; ?>
                                            </td>
                                            <td data-title="jawaban-salah">
                                                <?php echo($num_of_answers - $num_of_correct_answers); ?>
                                            </td>
                                            <td data-title="score-jawaban-benar">
                                                <?php echo $scr_of_correct_answers; ?>
                                            </td>
                                            <td data-title="pinalty-kesalahan-per-kolom">
                                                <?php echo $negative_point; ?>
                                            </td>
                                            <td data-title="score">
                                                <?php echo number_format($percentage_of_point, 2, '.', ''); ?>
                                            </td>
                                            <td data-title="graph-down">
                                                <?php if ($graph_down) echo $graph_down; ?>
                                            </td>
                                            <td data-title="Laporan">
                                                <?php if($onlineExam->is_show_answer == 1){ ?>
                                                    <button id="get_question_list" class="btn btn-success" onclick="newPopup('<?=base_url("take_exam/reporttest/{$exams['onlineExamID']}/{$exams['userID']}/{$exams['examtimeID']}")?>')" style="margin-top:2px;">Jawaban</a></button>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } 
                                    
                                    // negative point for turn down graph
                                    $negative_point_desc = $totalNegativePoint." + ";
                                    if ($num_of_turn_down == 1) {
                                        $totalScoreTurnDown = 1;
                                        $negative_point_desc .= 1;
                                    }
                                    else if ($num_of_turn_down == 2) {
                                        $totalScoreTurnDown = 2;
                                        $negative_point_desc .= 2;
                                    }
                                    else if ($num_of_turn_down == 3) {
                                        $totalScoreTurnDown = 3;
                                        $negative_point_desc .= 3;
                                    }
                                    else if ($num_of_turn_down == 4) {
                                        $totalScoreTurnDown = 4;
                                        $negative_point_desc .= 4;
                                    }
                                    else if ($num_of_turn_down == 5) {
                                        $totalScoreTurnDown = 5;
                                        $negative_point_desc .= 5;
                                    }
                                    else if ($num_of_turn_down > 5) {
                                        $totalScoreTurnDown = 10;
                                        $negative_point_desc .= 10;
                                    }
                                    $negative_point_desc .= " (grafik turun ".$num_of_turn_down."x)";

                                    if ($i > 0) $totalPercCorrectAnswer /= $i;
                                    $totalPerc = $totalPercCorrectAnswer - $totalNegativePoint - $totalScoreTurnDown;
                                    ?>
                                    
                                    <tr row="1">
                                        <td data-title="No.">
                                            -
                                        </td>
                                        <td data-title="Master">
                                            <b>Nilai Total</b>
                                        </td>
                                        <td data-title="jawaban-benar"></td>
                                        <td data-title="jawaban-salah"></td>
                                        <td data-title="score-jawaban-benar"></td>
                                        <td data-title="pinalty-kesalahan-per-kolom">
                                            <h5><?php echo $totalNegativePoint; ?></h5>
                                        </td>
                                        <td data-title="score">
                                            <h5><?php echo number_format($totalPercCorrectAnswer, 2, '.', ''); ?></h5>
                                        </td>
                                        <td data-title="graph-down">
                                            <h5><?php echo $totalScoreTurnDown; ?></h5>
                                        </td>
                                        
                                        <td data-title="Laporan">
                                            <?php 
                                                echo "<h4>".number_format($totalPerc, 2, '.', '')."</h4>"; 
                                                if($totalPerc <= 50)
                                                    $predikat = "Kurang";
                                                else if($totalPerc >= 51 && $totalPerc <= 69)
                                                    $predikat = "Cukup";
                                                else if($totalPerc > 70 && $totalPerc <= 85)
                                                    $predikat = "Baik";
                                                else if($totalPerc >= 86 && $totalPerc <= 100)
                                                    $predikat = "Sangat Baik";
                                                else
                                                    $predikat = "Sangat Kurang";
                                                
                                                if(($totalPerc / $i) >= 70) {
                                                    echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . ' dengan Predikat '.$predikat.'</span>';
                                                } else {
                                                    echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . ' dengan Predikat '.$predikat.'</span>';
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
				
                <div class="col-sm-12 col-md-12 col-lg-12" id="containerNilai"></div>
				<div class="col-sm-12 text-center footerAll" style="margin-bottom: 25px;">
                    <?=reportfooter($siteinfos)?>
                </div>
            </div><!-- row -->
        </div><!-- Body -->
    </div>
</div>
<script type="application/javascript">
    function newPopup(url) {
        var myWindowStatus = false;
        
		myWindowStatus = true;
		myWindow = window.open(url,'_blank',"width=1000,height=650,toolbar=0,location=0,scrollbars=yes");
		runner();
	}
</script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
	Highcharts.chart('containerNilai', {
		
		title: {
			text: 'Grafik Nilai'
		},

		subtitle: {
			text: ''
		},

		yAxis: {
			title: {
				text: 'Nilai'
			}
		},

		xAxis: {
			title: {
				text: 'Master Soal'
			},
			categories: <?= json_encode($arrChart['master']); ?>
		},

		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle'
		},


		series: [{
			name: 'Score',
			data: <?= json_encode($arrChart['totalObtainedMark']); ?>
		}, {
			name: 'Total Jawaban Benar',
			data: <?= json_encode($arrChart['totalCurrectAnswer']); ?>
		}],

		responsive: {
			rules: [{
				condition: {
					maxWidth: 400
				},
				chartOptions: {
					legend: {
						layout: 'horizontal',
						align: 'center',
						verticalAlign: 'bottom'
					}
				}
			}]
		}

	});
</script>