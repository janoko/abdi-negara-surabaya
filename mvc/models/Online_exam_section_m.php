<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_exam_section_m extends MY_Model {

	protected $_table_name = 'online_exam_section';
	protected $_primary_key = 'onlineExamSectionID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "onlineExamSectionID asc";

	public function __construct()
	{
		parent::__construct();
	}

	public function get_sections($online_exam_id)
	{
		$this->db->select('*');
		$this->db->from($this->_table_name);
		$this->db->join('section', $this->_table_name.'.sectionID = section.sectionID', 'LEFT');
		$this->db->where([$this->_table_name.".onlineExamID" => $online_exam_id]);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_online_exam($online_exam_id)
	{
		$query = parent::get_order_by(["onlineExamID" => $online_exam_id]);
		return $query;
	}

	public function insert_section($array)
	{
		$error = parent::insert($array);
		return TRUE;
	}

	public function insert_for_online_exam($onlineExamId, $sectionsId)
	{
		if(!$sectionsId) return FALSE;
		foreach ($sectionsId as $key => $value) {
			$data = [
				"onlineExamID" => $onlineExamId,
				"sectionID" => $value
			];
			$this->insert_section($data);
		}
		return TRUE;
	}

	public function update_sections($onlineExamId, $sectionsId)
	{
		$this->delete_by_online_exam($onlineExamId);
		$this->insert_for_online_exam($onlineExamId, $sectionsId);
		return TRUE;
	}

	public function delete_by_online_exam($onlineExamId)
	{
		parent::delete_where(["onlineExamID" => $onlineExamId]);
	}
}