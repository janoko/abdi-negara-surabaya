<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kode_verifikasi_m extends MY_Model {

	protected $_table_name = 'kode_verifikasi';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = "created_at desc";
	private $today = NULL;
	private $now = NULL;

	private $varification_error = NULL;

	public function __construct()
	{
		parent::__construct();
		$now = strtotime('now');
		$this->now = date("Y-m-d h:i:s", $now);
		$this->today = date("Y-m-d", $now);
	}
	
	public function get_last_aday_unused_kode($student_id)
	{
		$this->db->select('*');
		$this->db->from($this->_table_name);
		$this->db->where([
			"student_id" => $student_id,
			"num_of_used" => 0
		]);
		$this->db->where('DATE(created_at) =', $this->today);
		$this->db->order_by('created_at desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_num_of_used_today($student_id)
	{
		$this->db->select('count(*) c');
		$this->db->from($this->_table_name);
		$this->db->where([
			"student_id" => $student_id
		]);
		$this->db->where('DATE(created_at) =', $this->today);
		$this->db->where('num_of_used !=', 0);
		$this->db->order_by('created_at desc');
		$query = $this->db->get();
		return intval($query->result()[0]->c);
	}

	public function verificate($student_id, $kode)
	{
		$this->varification_error = NULL;
		$this->db->select('*');
		$this->db->from($this->_table_name);
		$this->db->where([
			"kode" => $kode,
			"student_id" => $student_id,
			"num_of_used" => 0
		]);
		$this->db->where('DATE(created_at) =', $this->today);
		$this->db->limit(1);
		$query = $this->db->get();
		$results = $query->result(); 
		if (count($results) == 0) return NULL;
		$this->kodeUsed($results[0]);
		return $results[0];
	}

	/**
	 * 1 kode cuma bisa digunakan sekali.
	 * dalam sehari (24h), 1 akun cuma bisa menggunakan 2 kodepin
	 */
	public function generate_by_student_id($student_id)
	{	
		$codes = $this->get_last_aday_unused_kode($student_id);
		if (count($codes)) return $codes[0];
		
		$num_of_used = $this->get_num_of_used_today($student_id);
		if($num_of_used >= 2)
		{
			return NULL;
		}

		$data = [
			"kode" => rand(0, 999999),
			"student_id" => $student_id,
			"created_at" => $this->now
		];
		$id = parent::insert($data);
		return parent::get($id, true);
	}

	public function kodeUsed($kodeData)
	{
		$isOk = parent::update([
			"num_of_used" => intval($kodeData->num_of_used) + 1
		], $kodeData->id);
		print_r($this->db->last_query());
	}
	
	public function kodeSent($kodeData)
	{
		
	}

	public function update($data, $id = NULL)
	{
		parent::update($data, $id);
		return $id;
	}

	public function delete_kodepin($id)
	{
		parent::delete($id);
	}

}