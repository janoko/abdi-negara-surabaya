<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_exam_class_m extends MY_Model {

	protected $_table_name = 'online_exam_class';
	protected $_primary_key = 'onlineExamClassID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "onlineExamClassID asc";

	public function __construct()
	{
		parent::__construct();
	}

	public function get_classes($online_exam_id)
	{
		$this->db->select('*');
		$this->db->from($this->_table_name);
		$this->db->join('classes', $this->_table_name.'.classID = classes.classesID', 'LEFT');
		$this->db->where([$this->_table_name.".onlineExamID" => $online_exam_id]);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_single_classes($array)
	{
        $query = parent::get_single($array);
        return $query;
    }

	public function get_by_online_exam($online_exam_id)
	{
		$query = parent::get_order_by(["onlineExamID" => $online_exam_id]);
		return $query;
	}

	public function insert_classes($array)
	{
		$error = parent::insert($array);
		return TRUE;
	}

	public function insert_for_online_exam($onlineExamId, $classesId)
	{
		if(!$classesId) return FALSE;
		foreach ($classesId as $key => $value) {
			$data = [
				"onlineExamID" => $onlineExamId,
				"classID" => $value
			];
			$this->insert_classes($data);
		}
		return TRUE;
	}

	public function update_classes($onlineExamId, $classesId)
	{
		$this->delete_by_online_exam($onlineExamId);
		$this->insert_for_online_exam($onlineExamId, $classesId);
		return TRUE;
	}

	public function delete_classes($id)
	{
		parent::delete($id);
	}

	public function delete_by_online_exam($onlineExamId)
	{
		parent::delete_where(["onlineExamID" => $onlineExamId]);
	}
}