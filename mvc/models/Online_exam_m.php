<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_exam_m extends MY_Model {

    protected $_table_name = 'online_exam';
    protected $_primary_key = 'onlineExamID';
    protected $_primary_filter = 'intval';
    protected $_order_by = "onlineExamID asc";

    public function __construct() 
    {
        parent::__construct();
    }

    public function get_online_exam($array=NULL, $signal=FALSE) 
    {
        $query = parent::get($array, $signal);
        return $query;
    }

    public function get_single_online_exam($array) 
    {
        $query = parent::get_single($array);
        return $query;
    }

    public function get_order_by_online_exam($array=NULL) 
    {
        $query = parent::get_order_by($array);
        return $query;
    }

    public function get_order_by_online_exam_on_classes($array=NULL, $classesID=NULL)
    {
        $cond = [];
        $cond_arr = [];
        foreach ($array as $key => $value) {
            if(gettype($value) != 'array'){
                $cond[$this->_table_name.'.'.$key] = $value;
            }else {
                $cond_arr[$this->_table_name.'.'.$key] = $value;
            }
        }
        $query = $this->db->select($this->_table_name.'.*')->from($this->_table_name);
		$query = $query->join('online_exam_class', $this->_table_name.'.onlineExamID = online_exam_class.onlineExamID', 'INNER');
        if($classesID){
            $query = $query->group_start()
                        // ->where(['online_exam_class.onlineExamClassID' => NULL])
                        ->or_where(["online_exam_class.classID" => $classesID])
                    ->group_end();
        }
        if(count($cond) > 0){
            $query = $query->where($cond);
        }
        if(count($cond_arr) > 0) {
            $query = $query->where_in($cond_arr);
        }
		$query = $query->order_by($this->_order_by);
		$query = $query->get();
		return $query->result();
    }

    public function insert_online_exam($array) 
    {
        $id = parent::insert($array);
        return $id;
    }

    public function update_online_exam($data, $id = NULL) 
    {
        parent::update($data, $id);
        return $id;
    }

    public function delete_online_exam($id)
    {
        parent::delete($id);
    }

    public function get_online_exam_by_student($array) 
    {
        $query = $this->db->select($this->_table_name.'.*')->from($this->_table_name);
        $query = $query->join('online_exam_class', $this->_table_name.'.onlineExamID = online_exam_class.onlineExamID', 'LEFT');
        $query = $query->join('online_exam_section', $this->_table_name.'.onlineExamID = online_exam_section.onlineExamID', 'LEFT');
        if(isset($array['classesID']) && $array['classesID'] && $array['classesID'] != '0'){
            $query = $query->group_start()
                        // ->where(['online_exam_class.onlineExamClassID' => NULL])
                        ->where(["online_exam_class.classID" => $array['classesID']])
                    ->group_end();
        }
        if (isset($array['sectionID']) && $array['sectionID'] && $array['sectionID'] != '0') {
            $query = $query->group_start()
                        ->where(["online_exam_section.sectionID" => $array['sectionID']])
                    ->group_end();
        }
        if (isset($array['onlineExamID']) && $array['onlineExamID']) {
            $query = $query->where([$this->_table_name.'.onlineExamID' => $array['onlineExamID']]);
        }
        $query = $query->where(['published' => '1']);
        $query = $query->limit(1);
        $result = $query->get();
        return $result->row();
    }
	
	public function get_online_exam_by_parent($parent) 
    {   
        $query = $this->db->from($this->_table_name)
                    ->where(['parentID' => $parent]);
        $result = $query->get();
        return $result->result();
    }
}
