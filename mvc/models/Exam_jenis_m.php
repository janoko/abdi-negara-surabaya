<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exam_jenis_m extends MY_Model {

    protected $_table_name = 'online_exam_jenis';
    protected $_primary_key = 'onlineExamJenisID';
    protected $_primary_filter = 'intval';
    protected $_order_by = "examJenisNumber asc";

    public function __construct() 
    {
        parent::__construct();
    }

    public function get_exam_jenis($array=NULL, $signal=FALSE) 
    {
        $query = parent::get($array, $signal);
        return $query;
    }

    public function get_single_exam_jenis($array) 
    {
        $query = parent::get_single($array);
        return $query;
    }

    public function get_order_by_exam_jenis($array=NULL) 
    {
        $query = parent::get_order_by($array);
        return $query;
    }

    public function insert_exam_jenis($array) 
    {
        $id = parent::insert($array);
        return $id;
    }

    public function update_exam_jenis($data, $id = NULL) 
    {
        parent::update($data, $id);
        return $id;
    }

    public function delete_exam_jenis($id)
    {
        parent::delete($id);
    }
}
