<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kodepin_m extends MY_Model {

	protected $_table_name = 'kodepin';
	protected $_primary_key = 'kodepinID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "create_date desc";

	public function __construct()
	{
		parent::__construct();
	}

	public function get_join_kodepin()
	{
		$this->db->select('*');
		$this->db->from('kodepin');
		$this->db->order_by('create_date asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function general_get_kodepin($array=NULL, $signal=FALSE)
	{
		$query = parent::get($array, $signal);
		return $query;
	}

	public function general_get_order_by_kodepin($array=NULL)
	{
		$query = parent::get_order_by($array);
		return $query;
	}

	public function get_kodepin($id=NULL, $signal=false)
	{
		$query = parent::get($id, $signal);
		return $query;
	}

	public function get_single_kodepin($array)
	{
        $query = parent::get_single($array);
        return $query;
    }

	public function get_order_by_kodepin($array=NULL)
	{
		$query = parent::get_order_by($array);
		return $query;
	}

	public function insert_kodepin($array)
	{
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_kodepin($data, $id = NULL)
	{
		parent::update($data, $id);
		return $id;
	}

	public function delete_kodepin($id)
	{
		parent::delete($id);
	}

	public function get_order_by_numeric_kodepin()
	{
		$this->db->select('*')->from('kodepin')->order_by('kodepin_numeric asc');
		$query = $this->db->get();
		return $query->result();
	}
}