<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_exam_user_status_m extends MY_Model {

    protected $_table_name = 'online_exam_user_status';
    protected $_primary_key = 'onlineExamUserStatus';
    protected $_primary_filter = 'intval';
    protected $_order_by = "totalObtainedMark desc";

    public function __construct() 
    {
        parent::__construct();
    }

    public function get_online_exam_user_status($array=NULL, $signal=FALSE) 
    {
        $query = parent::get($array, $signal);
        return $query;
    }

    public function get_single_online_exam_user_status($array) 
    {
        $query = parent::get_single($array);
        return $query;
    }

    public function get_order_by_online_exam_user_status($array=NULL) 
    {
        $query = parent::get_order_by($array);
        return $query;
    }

    public function get_order_by_parent_online_exam_user_status($queryArray) 
    {
        $queryArray['parentID'] = $queryArray['onlineexamID'];
        unset($queryArray['onlineexamID']);
        
        $query = $this->db->from('online_exam')->select(['count(*) as num_of_kolom'])
                    ->where([
                        'parentID' => $queryArray['parentID']
                    ])->get();
        $num_of_kolom = $query->result()[0]->num_of_kolom;

        $query = $this->db->from($this->_table_name)->select(['*', 'count(*) as num_of_kolom']);
        $query = $query->where($queryArray);
        $query = $query->group_by(["userID","examtimeID"]);
        $query = $query->having(["num_of_kolom" => $num_of_kolom]);
        $query = $query->get();
        return $query->result();
    }

    public function insert_online_exam_user_status($array) 
    {
        $id = parent::insert($array);
        return $id;
    }

    public function update_online_exam_user_status($data, $id = NULL) 
    {
        parent::update($data, $id);
        return $id;
    }

    public function delete_online_exam_user_status($id) 
    {
        parent::delete($id);
    }

    public function get_new_examtime_by_parent($parentID, $onlineExamID, $userID)
    {
        $cond = ['userID' => $userID];

        if ($parentID == 0)
            $cond['onlineExamID'] = $onlineExamID;
        else
            $cond['parentID'] = $parentID;

        $query = $this->db->from($this->_table_name)->select(['examtimeID', 'onlineExamID']);
        $query = $query->where($cond);
        $query = $query->limit(1);
        $query = $query->order_by('onlineExamUserStatus desc');
        $query = $query->get();
        $results = $query->result();

        $examtimeID = 1;
        if (count($results)) {
            $examtimeID = intval($results[0]->examtimeID);
            if ($parentID == 0 || intval($results[0]->onlineExamID) >= $onlineExamID)
                $examtimeID++;
        }
        return $examtimeID;
    }
}
