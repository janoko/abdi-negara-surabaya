<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_exam_sub_m extends MY_Model {

    protected $_table_name = 'online_exam';
    protected $_primary_key = 'onlineExamID';
    protected $_primary_filter = 'intval';
    protected $_order_by = "onlineExamID desc";

    public function __construct() 
    {
        parent::__construct();
    }

    public function get_online_exam_sub($array=NULL, $signal=FALSE) 
    {
        $query = parent::get($array, $signal);
        return $query;
    }

    public function get_single_online_exam_sub($array) 
    {
        $query = parent::get_single($array);
        return $query;
    }

    public function get_order_by_online_exam_sub($array=NULL) 
    {
        $query = parent::get_order_by($array);
        return $query;
    }

    public function insert_online_exam_sub($array) 
    {
        $id = parent::insert($array);
        return $id;
    }

    public function update_online_exam_sub($data, $id = NULL) 
    {
        parent::update($data, $id);
        return $id;
    }

    public function delete_online_exam_sub($id)
    {
        parent::delete($id);
    }

    public function get_online_exam_sub_by_student($array) 
    {
        /**
         * TODO [ggg]
         * Class and section is multyple check all file used this method
         */

        $query = "SELECT * FROM online_exam WHERE (classID='".$array['classesID']."' || classID='0') && (sectionID='".$array['sectionID']."' || sectionID='0') && published='1' && onlineExamID='".$array['onlineExamID']."'";
        $result = $this->db->query($query);
        return $result->row();
    }

}
