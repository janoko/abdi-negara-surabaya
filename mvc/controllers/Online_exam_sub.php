<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class online_exam_sub_sub extends Admin_Controller {
    /*
    | -----------------------------------------------------
    | PRODUCT NAME: 	INILABS SCHOOL MANAGEMENT SYSTEM
    | -----------------------------------------------------
    | AUTHOR:			INILABS TEAM
    | -----------------------------------------------------
    | EMAIL:			info@inilabs.net
    | -----------------------------------------------------
    | COPYRIGHT:		RESERVED BY INILABS IT
    | -----------------------------------------------------
    | WEBSITE:			http://inilabs.net
    | -----------------------------------------------------
    */
    function __construct() {
        parent::__construct();
        $this->load->model("online_exam_sub_m");
        
    }

    public function index() {
        
        $this->data["subview"] = "online_exam_sub/index";
        $this->load->view('_layout_main', $this->data);
    }

  

}
