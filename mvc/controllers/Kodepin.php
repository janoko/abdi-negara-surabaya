<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kodepin extends Admin_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	INILABS SCHOOL MANAGEMENT SYSTEM
| -----------------------------------------------------
| AUTHOR:			INILABS TEAM
| -----------------------------------------------------
| EMAIL:			info@inilabs.net
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY INILABS IT
| -----------------------------------------------------
| WEBSITE:			http://inilabs.net
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
        $this->load->model("kodepin_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('kodepin', $language);	
	}

	protected function rules() {
		$rules = array(
			array(
				'field' => 'kodepin', 
				'label' => $this->lang->line("kodepin_name"), 
				'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_kodepin'
			)
		);
		return $rules;
	}

	public function index() {
		$this->data['kodepin'] = $this->kodepin_m->get_join_kodepin();
		$this->data["subview"] = "kodepin/index";
		$this->load->view('_layout_main', $this->data);
	}

	public function add() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/select2/css/select2.css',
				'assets/select2/css/select2-bootstrap.css'
			),
			'js' => array(
				'assets/select2/select2.js'
			)
		);

		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) { 
				$this->data["subview"] = "kodepin/add";
				$this->load->view('_layout_main', $this->data);			
			} else {
				$array = array(
					"kodepin" => $this->input->post("kodepin"),
					"quota" => $this->input->post("quota"),
					"create_date" => date("Y-m-d h:i:s"),
					"modify_date" => date("Y-m-d h:i:s"),
					"create_userID" => $this->session->userdata('loginuserID'),
					"create_username" => $this->session->userdata('username'),
					"create_usertype" => $this->session->userdata('usertype'),
					"status" => 1,
				);

				$this->kodepin_m->insert_kodepin($array);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("kodepin/index"));
			}
		} else {
			$this->data["subview"] = "kodepin/add";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {

		$id = htmlentities(escapeString($this->uri->segment(3)));
		if((int)$id) {
			$this->data['kodepin'] = $this->kodepin_m->get_kodepin($id);
			if($this->data['kodepin']) {

				if($_POST) {
					$rules = $this->rules();
					$this->form_validation->set_rules($rules);
					if ($this->form_validation->run() == FALSE) {
						$this->data["subview"] = "kodepin/edit";
						$this->load->view('_layout_main', $this->data);			
					} else {
						$array = array(
							"kodepin" => $this->input->post("kodepin"),
							"quota" => $this->input->post("quota"),
							"modify_date" => date("Y-m-d h:i:s")
						);

						$this->kodepin_m->update_kodepin($array, $id);
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("kodepin/index"));
					}
				} else {
					$this->data["subview"] = "kodepin/edit";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function active() {
		if(permissionChecker('kodepin_edit')) {
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			if($id != '' && $status != '') {
				if((int)$id) {
					if($status == 'chacked') {
						$this->kodepin_m->update_kodepin(array('status' => 1), $id);
						echo 'Success';
					} elseif($status == 'unchacked') {
						$this->kodepin_m->update_kodepin(array('status' => 0), $id);
						echo 'Success';
					} else {
						echo "Error";
					}
				} else {
					echo "Error";
				}
			} else {
				echo "Error";
			}
		} else {
			echo "Error";
		}
	}
	
	public function delete() {
		$id = htmlentities(escapeString($this->uri->segment(3)));
		if((int)$id) {
			$this->kodepin_m->delete_kodepin($id);
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			redirect(base_url("kodepin/index"));
		} else {
			redirect(base_url("kodepin/index"));
		}
	}
	
	public function unique_kodepin() {
		$id = htmlentities(escapeString($this->uri->segment(3)));
		if((int)$id) {
			$kodepin = $this->kodepin_m->get_order_by_kodepin(array("kodepin" => $this->input->post("kodepin"), "kodepinID !=" => $id));
			if(inicompute($kodepin)) {
				$this->form_validation->set_message("unique_kodepin", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$kodepin = $this->kodepin_m->get_order_by_kodepin(array("kodepin" => $this->input->post("kodepin")));

			if(inicompute($kodepin)) {
				$this->form_validation->set_message("unique_kodepin", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}	
	}

}