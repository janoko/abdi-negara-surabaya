<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Omnipay\Omnipay;
Class Do_try extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model("signin_m");
        $this->load->model("permission_m");
        $this->load->model("site_m");
        $this->load->model("schoolyear_m");
        $this->load->model("alert_m");
        $this->load->model("menu_m");
        $this->load->model('usertype_m');
        $this->load->model("section_m");
        $this->load->library("session");
        $this->load->helper('language');
        $this->load->helper('date');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->lang->load('topbar_menu', $this->session->userdata('lang'));

        $module            = $this->uri->segment(1);
        $action            = $this->uri->segment(2);
        $siteInfo          = $this->site_m->get_site();

        $this->data["siteinfos"]         = $siteInfo;
        $this->_backendTheme             = strtolower($this->data["siteinfos"]->backend_theme);
        $this->_backendThemePath         = 'assets/inilabs/themes/' . strtolower($this->data["siteinfos"]->backend_theme);
        $this->data['backendTheme']      = $this->_backendTheme;
        $this->data['backendThemePath']  = $this->_backendThemePath;
        $this->data['topbarschoolyears'] = $this->schoolyear_m->get_order_by_schoolyear([ 'schooltype' => $this->data["siteinfos"]->school_type ]);
        $this->data['allcountry']        = $this->_country();
        $this->data['allbloodgroup']     = $this->_bloodGroup();
		
        $this->load->model('online_exam_m');
        $this->load->model('online_exam_class_m');
        $this->load->model('online_exam_section_m');
        $this->load->model('online_exam_payment_m');
        $this->load->model('online_exam_question_m');
        $this->load->model('instruction_m');
        $this->load->model('question_bank_m');
        $this->load->model('question_option_m');
        $this->load->model('question_answer_m');
        $this->load->model('online_exam_user_answer_m');
        $this->load->model('online_exam_user_status_m');
        $this->load->model('online_exam_user_answer_option_m');
        $this->load->model('student_m');
        $this->load->model('classes_m');
        $this->load->model('section_m');
        $this->load->model('paymentsettings_m');
        $this->load->model('subject_m');
        require_once(APPPATH."libraries/Omnipay/vendor/autoload.php");
    }

    public function move_oe_class() {
        $exams = $this->online_exam_m->get();
        foreach ($exams as $key => $exam) {
            print_r($exam->onlineExamID);
            print("-");
            print_r($exam->classID);
            print("-");
            $this->online_exam_class_m->delete_by_online_exam($exam->onlineExamID);
            print($this->online_exam_class_m->insert_for_online_exam($exam->onlineExamID, [$exam->classID]));
            print("<br />");
        }
    }

    public function move_oe_section() {
        $exams = $this->online_exam_m->get();
        foreach ($exams as $key => $exam) {
            print_r($exam->onlineExamID);
            print("-");
            print_r($exam->sectionID);
            print("-");
            print($this->online_exam_section_m->update_sections($exam->onlineExamID, [$exam->sectionID]));
            print("<br />");
        }
    }

    public function show_exam() {
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/checkbox/checkbox.css',
                'assets/inilabs/form/fuelux.min.css'
            )
        );
        $this->data['footerassets'] = array(
            'js' => array(
                'assets/inilabs/form/fuelux.min.js'
            )
        );


        $userID = 280;
        $onlineExamID = htmlentities(escapeString($this->uri->segment(3)));

        $examGivenStatus     = FALSE;
        $examGivenDataStatus = FALSE;
        $examExpireStatus    = FALSE;
        $examSubjectStatus   = FALSE;

        if((int) $onlineExamID) {
            $this->data['student'] = $this->student_m->get_student($userID);
            if(inicompute($this->data['student'])) {
                $array['classesID'] = $this->data['student']->classesID;
                $array['sectionID'] = $this->data['student']->sectionID;
                $array['studentgroupID'] = $this->data['student']->studentgroupID;
                $array['onlineExamID'] = $onlineExamID;
                $online_exam = $this->online_exam_m->get_online_exam_by_student($array);


                $userExamCheck = $this->online_exam_user_status_m->get_order_by_online_exam_user_status(
                    array(
                        'userID'=>$userID,
                        'classesID'=>$array['classesID'],
                        'sectionID'=>$array['sectionID'],
                        'onlineExamID'=> $onlineExamID
                    )
                );
                if(inicompute($online_exam)) {
                    $DDonlineExam = $online_exam;
                    $DDexamStatus = $userExamCheck;

                    $currentdate = 0;
                    if($DDonlineExam->examTypeNumber == '4') {
                        $presentDate = strtotime(date('Y-m-d'));
                        $examStartDate = strtotime($DDonlineExam->startDateTime);
                        $examEndDate = strtotime($DDonlineExam->endDateTime);
                    } elseif($DDonlineExam->examTypeNumber == '5') {
                        $presentDate = strtotime(date('Y-m-d H:i:s'));
                        $examStartDate = strtotime($DDonlineExam->startDateTime);
                        $examEndDate = strtotime($DDonlineExam->endDateTime);
                    }

                    if($DDonlineExam->examTypeNumber == '4' || $DDonlineExam->examTypeNumber == '5') {
                        if($presentDate >= $examStartDate && $presentDate <= $examEndDate) {
                            $examGivenStatus = TRUE;
                        } elseif($presentDate > $examStartDate && $presentDate > $examEndDate) {
                            $examExpireStatus = TRUE;
                        }
                    } else {
                        $examGivenStatus = TRUE;
                    }

                    if($examGivenStatus) {
                        $examGivenStatus = FALSE;
                        if($DDonlineExam->examStatus == 2) {
                            $examGivenStatus = TRUE;
                        } else {
                            $userExamCheck = pluck($userExamCheck,'obj','onlineExamID');
                            if(isset($userExamCheck[$DDonlineExam->onlineExamID])) {
                                $examGivenDataStatus = TRUE;
                            } else {
                                $examGivenStatus = TRUE;
                            }
                        }
                    }

                    if($examGivenStatus) {
                        if((int)$DDonlineExam->subjectID && (int)$DDonlineExam->classID) {
                            $examGivenStatus = FALSE;
                            $userSubjectPluck = pluck($this->subject_m->get_order_by_subject(array('type' => 1)), 'subjectID', 'subjectID');
                            $optionalSubject = $this->subject_m->get_single_subject(array('type' => 0, 'subjectID' => $this->data['student']->optionalsubjectID));
                            if(inicompute($optionalSubject)) {
                                $userSubjectPluck[$optionalSubject->subjectID] = $optionalSubject->subjectID;
                            }

                            if(in_array($DDonlineExam->subjectID, $userSubjectPluck)) {
                                $examGivenStatus = TRUE;
                            } else {
                                $examSubjectStatus = FALSE;
                            }
                        } else {
                            $examSubjectStatus = TRUE;
                        }
                    } else {
                        $examSubjectStatus = TRUE;
                    }
                }
                $this->data['class'] = $this->classes_m->get_classes($this->data['student']->classesID);
            } else {
                $this->data['class'] = array();
            }

            if(inicompute($this->data['student'])) {
                $this->data['section'] = $this->section_m->get_section($this->data['student']->sectionID);
            } else {
                $this->data['section'] = array();
            }

            $this->data['onlineExam'] = $this->online_exam_m->get_single_online_exam(['onlineExamID' => $onlineExamID]);
            if(inicompute($online_exam)) {
                $onlineExamQuestions = $this->online_exam_question_m->get_order_by_online_exam_question(['onlineExamID' => $onlineExamID]);

                $allOnlineExamQuestions = $onlineExamQuestions;

                //if($this->data['onlineExam']->random != 0) {
                   // $onlineExamQuestions = $this->randAssociativeArray($onlineExamQuestions, $this->data['onlineExam']->random);
                //}

                $this->data['onlineExamQuestions'] = $onlineExamQuestions;
                // $this->data['ssss'] = $this->db->last_query();
                // $this->data['dfff'] = count($onlineExamQuestions);
                $onlineExamQuestions = pluck($onlineExamQuestions, 'obj', 'questionID');
                $questionsBank = pluck(
                    $this->question_bank_m->get_where_in_question_bank(pluck($onlineExamQuestions, 'questionID'), 'questionBankID'), 
                    'obj', 'questionBankID'
                );
                $this->data['questions'] = $questionsBank;
                // echo "<!-- ";
                // print_r(pluck($onlineExamQuestions, 'questionID'));
                // print_r($questionsBank);
                // echo " -->";


                $options = [];
                $answers = [];
                $allOptions = [];
                $allAnswers = [];
                if(inicompute($allOnlineExamQuestions)) {
                    $pluckOnlineExamQuestions = pluck($allOnlineExamQuestions, 'questionID');
                    $allOptions = $this->question_option_m->get_where_in_question_option($pluckOnlineExamQuestions, 'questionID');
                    foreach ($allOptions as $option) {
                        if($option->name == "" && $option->img == "") continue;
                        $options[$option->questionID][] = $option;
                    }
                    $this->data['options'] = $options;
                    
                    $allAnswers = $this->question_answer_m->get_where_in_question_answer($pluckOnlineExamQuestions, 'questionID');
                    foreach ($allAnswers as $answer) {
                        $answers[$answer->questionID][] = $answer;
                    }
                    $this->data['answers'] = $answers;
                } else {
                    $this->data['options'] = $options;
                    $this->data['answers'] = $answers;
                }

                if($examGivenStatus) {
                    $this->data["subview"] = "online_exam/take_exam/question";
                    return $this->load->view('_layout_do_try', $this->data);
                } else {
                    if($examGivenDataStatus) {
                        $this->data['online_exam'] = $online_exam;
                        $userExamCheck = pluck($userExamCheck,'obj','onlineExamID');
                        $this->data['userExamCheck'] = isset($userExamCheck[$onlineExamID]) ? $userExamCheck[$onlineExamID] : [];
                        $this->data["subview"] = "online_exam/take_exam/checkexam";
                        return $this->load->view('_layout_do_try', $this->data);
                    } else {
                        if($examExpireStatus) {
                            $this->data['examsubjectstatus'] = $examSubjectStatus;
                            $this->data['expirestatus'] = $examExpireStatus;
                            $this->data['upcomingstatus'] = FALSE;
                            $this->data['online_exam'] = $online_exam;
                            $this->data["subview"] = "online_exam/take_exam/expireandupcoming";
                            return $this->load->view('_layout_do_try', $this->data);
                        } else {
                            $this->data['examsubjectstatus'] = $examSubjectStatus;
                            $this->data['expirestatus'] = $examExpireStatus;
                            $this->data['upcomingstatus'] = TRUE;
                            $this->data['online_exam'] = $online_exam;
                            $this->data["subview"] = "online_exam/take_exam/expireandupcoming";
                            return $this->load->view('_layout_do_try', $this->data);
                        }
                    }
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_do_try', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_do_try', $this->data);
        }
    }

    public function _country()
    {
        $country = [
            "AF" => "Afghanistan",
            "AL" => "Albania",
            "DZ" => "Algeria",
            "AS" => "American Samoa",
            "AD" => "Andorra",
            "AO" => "Angola",
            "AI" => "Anguilla",
            "AQ" => "Antarctica",
            "AG" => "Antigua and Barbuda",
            "AR" => "Argentina",
            "AM" => "Armenia",
            "AW" => "Aruba",
            "AU" => "Australia",
            "AT" => "Austria",
            "AZ" => "Azerbaijan",
            "BS" => "Bahamas",
            "BH" => "Bahrain",
            "BD" => "Bangladesh",
            "BB" => "Barbados",
            "BY" => "Belarus",
            "BE" => "Belgium",
            "BZ" => "Belize",
            "BJ" => "Benin",
            "BM" => "Bermuda",
            "BT" => "Bhutan",
            "BO" => "Bolivia",
            "BA" => "Bosnia and Herzegovina",
            "BW" => "Botswana",
            "BV" => "Bouvet Island",
            "BR" => "Brazil",
            "BQ" => "British Antarctic Territory",
            "IO" => "British Indian Ocean Territory",
            "VG" => "British Virgin Islands",
            "BN" => "Brunei",
            "BG" => "Bulgaria",
            "BF" => "Burkina Faso",
            "BI" => "Burundi",
            "KH" => "Cambodia",
            "CM" => "Cameroon",
            "CA" => "Canada",
            "CT" => "Canton and Enderbury Islands",
            "CV" => "Cape Verde",
            "KY" => "Cayman Islands",
            "CF" => "Central African Republic",
            "TD" => "Chad",
            "CL" => "Chile",
            "CN" => "China",
            "CX" => "Christmas Island",
            "CC" => "Cocos [Keeling] Islands",
            "CO" => "Colombia",
            "KM" => "Comoros",
            "CG" => "Congo - Brazzaville",
            "CD" => "Congo - Kinshasa",
            "CK" => "Cook Islands",
            "CR" => "Costa Rica",
            "HR" => "Croatia",
            "CU" => "Cuba",
            "CY" => "Cyprus",
            "CZ" => "Czech Republic",
            "CI" => "Côte d’Ivoire",
            "DK" => "Denmark",
            "DJ" => "Djibouti",
            "DM" => "Dominica",
            "DO" => "Dominican Republic",
            "NQ" => "Dronning Maud Land",
            "DD" => "East Germany",
            "EC" => "Ecuador",
            "EG" => "Egypt",
            "SV" => "El Salvador",
            "GQ" => "Equatorial Guinea",
            "ER" => "Eritrea",
            "EE" => "Estonia",
            "ET" => "Ethiopia",
            "FK" => "Falkland Islands",
            "FO" => "Faroe Islands",
            "FJ" => "Fiji",
            "FI" => "Finland",
            "FR" => "France",
            "GF" => "French Guiana",
            "PF" => "French Polynesia",
            "TF" => "French Southern Territories",
            "FQ" => "French Southern and Antarctic Territories",
            "GA" => "Gabon",
            "GM" => "Gambia",
            "GE" => "Georgia",
            "DE" => "Germany",
            "GH" => "Ghana",
            "GI" => "Gibraltar",
            "GR" => "Greece",
            "GL" => "Greenland",
            "GD" => "Grenada",
            "GP" => "Guadeloupe",
            "GU" => "Guam",
            "GT" => "Guatemala",
            "GG" => "Guernsey",
            "GN" => "Guinea",
            "GW" => "Guinea-Bissau",
            "GY" => "Guyana",
            "HT" => "Haiti",
            "HM" => "Heard Island and McDonald Islands",
            "HN" => "Honduras",
            "HK" => "Hong Kong SAR China",
            "HU" => "Hungary",
            "IS" => "Iceland",
            "IN" => "India",
            "ID" => "Indonesia",
            "IR" => "Iran",
            "IQ" => "Iraq",
            "IE" => "Ireland",
            "IM" => "Isle of Man",
            "IL" => "Israel",
            "IT" => "Italy",
            "JM" => "Jamaica",
            "JP" => "Japan",
            "JE" => "Jersey",
            "JT" => "Johnston Island",
            "JO" => "Jordan",
            "KZ" => "Kazakhstan",
            "KE" => "Kenya",
            "KI" => "Kiribati",
            "KW" => "Kuwait",
            "KG" => "Kyrgyzstan",
            "LA" => "Laos",
            "LV" => "Latvia",
            "LB" => "Lebanon",
            "LS" => "Lesotho",
            "LR" => "Liberia",
            "LY" => "Libya",
            "LI" => "Liechtenstein",
            "LT" => "Lithuania",
            "LU" => "Luxembourg",
            "MO" => "Macau SAR China",
            "MK" => "Macedonia",
            "MG" => "Madagascar",
            "MW" => "Malawi",
            "MY" => "Malaysia",
            "MV" => "Maldives",
            "ML" => "Mali",
            "MT" => "Malta",
            "MH" => "Marshall Islands",
            "MQ" => "Martinique",
            "MR" => "Mauritania",
            "MU" => "Mauritius",
            "YT" => "Mayotte",
            "FX" => "Metropolitan France",
            "MX" => "Mexico",
            "FM" => "Micronesia",
            "MI" => "Midway Islands",
            "MD" => "Moldova",
            "MC" => "Monaco",
            "MN" => "Mongolia",
            "ME" => "Montenegro",
            "MS" => "Montserrat",
            "MA" => "Morocco",
            "MZ" => "Mozambique",
            "MM" => "Myanmar [Burma]",
            "NA" => "Namibia",
            "NR" => "Nauru",
            "NP" => "Nepal",
            "NL" => "Netherlands",
            "AN" => "Netherlands Antilles",
            "NT" => "Neutral Zone",
            "NC" => "New Caledonia",
            "NZ" => "New Zealand",
            "NI" => "Nicaragua",
            "NE" => "Niger",
            "NG" => "Nigeria",
            "NU" => "Niue",
            "NF" => "Norfolk Island",
            "KP" => "North Korea",
            "VD" => "North Vietnam",
            "MP" => "Northern Mariana Islands",
            "NO" => "Norway",
            "OM" => "Oman",
            "PC" => "Pacific Islands Trust Territory",
            "PK" => "Pakistan",
            "PW" => "Palau",
            "PS" => "Palestinian Territories",
            "PA" => "Panama",
            "PZ" => "Panama Canal Zone",
            "PG" => "Papua New Guinea",
            "PY" => "Paraguay",
            "YD" => "People's Democratic Republic of Yemen",
            "PE" => "Peru",
            "PH" => "Philippines",
            "PN" => "Pitcairn Islands",
            "PL" => "Poland",
            "PT" => "Portugal",
            "PR" => "Puerto Rico",
            "QA" => "Qatar",
            "RO" => "Romania",
            "RU" => "Russia",
            "RW" => "Rwanda",
            "RE" => "Réunion",
            "BL" => "Saint Barthélemy",
            "SH" => "Saint Helena",
            "KN" => "Saint Kitts and Nevis",
            "LC" => "Saint Lucia",
            "MF" => "Saint Martin",
            "PM" => "Saint Pierre and Miquelon",
            "VC" => "Saint Vincent and the Grenadines",
            "WS" => "Samoa",
            "SM" => "San Marino",
            "SA" => "Saudi Arabia",
            "SN" => "Senegal",
            "RS" => "Serbia",
            "CS" => "Serbia and Montenegro",
            "SC" => "Seychelles",
            "SL" => "Sierra Leone",
            "SG" => "Singapore",
            "SK" => "Slovakia",
            "SI" => "Slovenia",
            "SB" => "Solomon Islands",
            "SO" => "Somalia",
            "ZA" => "South Africa",
            "GS" => "South Georgia and the South Sandwich Islands",
            "KR" => "South Korea",
            "ES" => "Spain",
            "LK" => "Sri Lanka",
            "SD" => "Sudan",
            "SR" => "Suriname",
            "SJ" => "Svalbard and Jan Mayen",
            "SZ" => "Swaziland",
            "SE" => "Sweden",
            "CH" => "Switzerland",
            "SY" => "Syria",
            "ST" => "São Tomé and Príncipe",
            "TW" => "Taiwan",
            "TJ" => "Tajikistan",
            "TZ" => "Tanzania",
            "TH" => "Thailand",
            "TL" => "Timor-Leste",
            "TG" => "Togo",
            "TK" => "Tokelau",
            "TO" => "Tonga",
            "TT" => "Trinidad and Tobago",
            "TN" => "Tunisia",
            "TR" => "Turkey",
            "TM" => "Turkmenistan",
            "TC" => "Turks and Caicos Islands",
            "TV" => "Tuvalu",
            "UM" => "U.S. Minor Outlying Islands",
            "PU" => "U.S. Miscellaneous Pacific Islands",
            "VI" => "U.S. Virgin Islands",
            "UG" => "Uganda",
            "UA" => "Ukraine",
            "SU" => "Union of Soviet Socialist Republics",
            "AE" => "United Arab Emirates",
            "GB" => "United Kingdom",
            "US" => "United States",
            "ZZ" => "Unknown or Invalid Region",
            "UY" => "Uruguay",
            "UZ" => "Uzbekistan",
            "VU" => "Vanuatu",
            "VA" => "Vatican City",
            "VE" => "Venezuela",
            "VN" => "Vietnam",
            "WK" => "Wake Island",
            "WF" => "Wallis and Futuna",
            "EH" => "Western Sahara",
            "YE" => "Yemen",
            "ZM" => "Zambia",
            "ZW" => "Zimbabwe",
            "AX" => "Åland Islands",
        ];
        return $country;
    }

    private function _bloodGroup()
    {
        $bloodGroup = [
            'A+'  => 'A+',
            'A-'  => 'A-',
            'B+'  => 'B+',
            'B-'  => 'B-',
            'O+'  => 'O+',
            'O-'  => 'O-',
            'AB+' => 'AB+',
            'AB-' => 'AB-'
        ];
        return $bloodGroup;
    }
}