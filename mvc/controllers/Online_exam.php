<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_exam extends Admin_Controller {
    /*
    | -----------------------------------------------------
    | PRODUCT NAME: 	INILABS SCHOOL MANAGEMENT SYSTEM
    | -----------------------------------------------------
    | AUTHOR:			INILABS TEAM
    | -----------------------------------------------------
    | EMAIL:			info@inilabs.net
    | -----------------------------------------------------
    | COPYRIGHT:		RESERVED BY INILABS IT
    | -----------------------------------------------------
    | WEBSITE:			http://inilabs.net
    | -----------------------------------------------------
    */
    function __construct() {
        parent::__construct();
        $this->load->model("online_exam_m");
        $this->load->model("online_exam_class_m");
        $this->load->model("online_exam_section_m");
        $this->load->model("classes_m");
        $this->load->model("section_m");
        $this->load->model("subject_m");
        $this->load->model("studentgroup_m");
        $this->load->model("usertype_m");
        $this->load->model("question_bank_m");
        $this->load->model("question_level_m");
        $this->load->model("question_group_m");
        $this->load->model("question_type_m");
        $this->load->model("question_option_m");
        $this->load->model("question_answer_m");
        $this->load->model("online_exam_question_m");
        $this->load->model("exam_type_m");
        $this->load->model("exam_jenis_m");
        $this->load->model("instruction_m");
        $this->load->model("student_m");
        $language = $this->session->userdata('lang');
        $this->lang->load('online_exam', $language);
    }

    public function index() {
        $usertypeID  = $this->session->userdata('usertypeID');
        $loginuserID = $this->session->userdata('loginuserID');

        $this->data['userSubjectPluck'] = [];
        if($usertypeID == '3') {
            $this->data['student'] = $this->student_m->get_single_student(array('studentID'=>$loginuserID));
            if(inicompute($this->data['student'])) {
                $this->data['userSubjectPluck'] = pluck($this->subject_m->get_order_by_subject(array('classesID'=> $this->data['student']->classesID, 'type' => 1)), 'subjectID', 'subjectID');
                $optionalSubject = $this->subject_m->get_single_subject(array('type' => 0, 'subjectID' => $this->data['student']->optionalsubjectID));
                if(inicompute($optionalSubject)) {
                    $this->data['userSubjectPluck'][$optionalSubject->subjectID] = $optionalSubject->subjectID;
                }
            }
        }

        $this->data['usertypeID']   = $usertypeID;
        $this->data['online_exams'] = $this->online_exam_m->get_order_by_online_exam(array('jenisExam'=>"parent"));

        $this->data['online_exams_classes'] = [];
        foreach ($this->data['online_exams'] as $online_exam) {
            $this->data['online_exams_classes'][$online_exam->onlineExamID] = $this->online_exam_class_m->get_classes($online_exam->onlineExamID);
        }

        $this->data["subview"] = "online_exam/index";
        $this->load->view('_layout_main', $this->data);
    }
	
	public function copydata(){
        // TODO [ggg]: class and section are multiple

		$this->data['headerassets'] = array(
            'css' => array(
                'assets/datepicker/datepicker.css',
                'assets/editor/jquery-te-1.4.0.css',
                'assets/select2/css/select2.css',
                'assets/select2/css/select2-bootstrap.css'
            ),
            'js' => array(
                'assets/editor/jquery-te-1.4.0.min.js',
                'assets/datepicker/datepicker.js',
                'assets/select2/select2.js'
            )
        );
        
        $onlineExamID = htmlentities(escapeString($this->uri->segment(3)));
        $onlineExamData = $this->online_exam_m->get_single_online_exam(array('onlineExamID'=>$onlineExamID));
		
		$array = [];
		$array['name'] = $onlineExamData->name;
		$array['jenisExam'] = $onlineExamData->jenisExam;
		$array['parentID'] = $onlineExamData->parentID;
		$array['description'] = $onlineExamData->description;
		$array['classID'] = $onlineExamData->classID;
		$array['sectionID'] = $onlineExamData->sectionID;
		$array['studentGroupID'] = $onlineExamData->studentGroupID;
		$array['subjectID'] = $onlineExamData->subjectID;
		$array['userTypeID'] = $onlineExamData->userTypeID;
		$array['instructionID'] = $onlineExamData->instructionID;
		$array['examStatus'] = $onlineExamData->examStatus;
		$array['random'] = $onlineExamData->random;
		$array['schoolYearID'] = $onlineExamData->schoolYearID;
		$array['examTypeNumber'] = $onlineExamData->examTypeNumber;
		$array['examJenisNumber'] = $onlineExamData->examJenisNumber;
		$array['startDateTime'] = $onlineExamData->startDateTime;
		$array['endDateTime'] = $onlineExamData->endDateTime;
		$array['duration'] = $onlineExamData->duration;
		$array['status'] = $onlineExamData->status;
		$array['markType'] = $onlineExamData->markType;
		$array['negativeMark'] = $onlineExamData->negativeMark;
		$array['bonusMark'] = $onlineExamData->bonusMark;
		$array['point'] = $onlineExamData->point;
		$array['percentage'] = $onlineExamData->percentage;
		$array['showMarkAfterExam'] = $onlineExamData->showMarkAfterExam;
		$array['judge'] = $onlineExamData->judge;
		$array['paid'] = $onlineExamData->paid;
		$array['validDays'] = $onlineExamData->validDays;
		$array['cost'] = $onlineExamData->cost;
		$array['create_date'] = $onlineExamData->create_date;
		$array['modify_date'] = $onlineExamData->modify_date;
		$array['create_userID'] = $onlineExamData->create_userID;
		$array['create_usertypeID'] = $onlineExamData->create_usertypeID;
		$array['published'] = $onlineExamData->published;

		$examInsertID = $this->online_exam_m->insert_online_exam($array);
		$onlineExamQuestion =  xGetRows("select * from online_exam_question where onlineExamID = {$onlineExamID}");
		foreach($onlineExamQuestion as $examQuests){
			$dataQuests['onlineExamID'] = $examInsertID;
			$dataQuests['questionID'] = $examQuests['questionID'];
			xInsertTable("online_exam_question",$dataQuests);
		}
		
        // jenis exam: sikap kerja
		if($onlineExamData->examJenisNumber == 5){
			$subOnlineExamQuestions =  xGetRows("select * from online_exam where parentID = {$onlineExamID}");
			foreach($subOnlineExamQuestions as $subExams){
				$sbarray = [];
				$sbarray['name'] = $subExams['name'];
				$sbarray['jenisExam'] = 'child';
				$sbarray['parentID'] = $examInsertID;
				$sbarray['description'] = $subExams['description'];
				$sbarray['classID'] = $subExams['classID'];
				$sbarray['sectionID'] = $subExams['sectionID'];
				$sbarray['studentGroupID'] = $subExams['studentGroupID'];
				$sbarray['subjectID'] = $subExams['subjectID'];
				$sbarray['userTypeID'] = $subExams['userTypeID'];
				$sbarray['instructionID'] = $subExams['instructionID'];
				$sbarray['examStatus'] = $subExams['examStatus'];
				$sbarray['random'] = $subExams['random'];
				$sbarray['schoolYearID'] = $subExams['schoolYearID'];
				$sbarray['examTypeNumber'] = $subExams['examTypeNumber'];
				$sbarray['examJenisNumber'] = $subExams['examJenisNumber'];
				$sbarray['startDateTime'] = $subExams['startDateTime'];
				$sbarray['endDateTime'] = $subExams['endDateTime'];
				$sbarray['duration'] = $subExams['duration'];
				$sbarray['status'] = $subExams['status'];
				$sbarray['markType'] = $subExams['markType'];
				$sbarray['negativeMark'] = $subExams['negativeMark'];
				$sbarray['bonusMark'] = $subExams['bonusMark'];
				$sbarray['point'] = $subExams['point'];
				$sbarray['percentage'] = $subExams['percentage'];
				$sbarray['showMarkAfterExam'] = $subExams['showMarkAfterExam'];
				$sbarray['judge'] = $subExams['judge'];
				$sbarray['paid'] = $subExams['paid'];
				$sbarray['validDays'] = $subExams['validDays'];
				$sbarray['cost'] = $subExams['cost'];
				$sbarray['create_date'] = $subExams['create_date'];
				$sbarray['modify_date'] = $subExams['modify_date'];
				$sbarray['create_userID'] = $subExams['create_userID'];
				$sbarray['create_usertypeID'] = $subExams['create_usertypeID'];
				$sbarray['published'] = $subExams['published'];

				$subExamInsertID = $this->online_exam_m->insert_online_exam($sbarray);
			}
		}
		// ECHO "<pre>";
		// print_r($onlineExamQuestion);
		// ECHO "</pre>";
		
		redirect(base_url("online_exam/index"));
	}
	
	function subexam($id = 0){
		$usertypeID  = $this->session->userdata('usertypeID');
        $loginuserID = $this->session->userdata('loginuserID');

        $this->data['userSubjectPluck'] = [];
        if($usertypeID == '3') {
            $this->data['student'] = $this->student_m->get_single_student(array('studentID'=>$loginuserID));
            if(inicompute($this->data['student'])) {
                $this->data['userSubjectPluck'] = pluck($this->subject_m->get_order_by_subject(array('classesID'=> $this->data['student']->classesID, 'type' => 1)), 'subjectID', 'subjectID');
                $optionalSubject = $this->subject_m->get_single_subject(array('type' => 0, 'subjectID' => $this->data['student']->optionalsubjectID));
                if(inicompute($optionalSubject)) {
                    $this->data['userSubjectPluck'][$optionalSubject->subjectID] = $optionalSubject->subjectID;
                }
            }
        }

        $this->data['usertypeID']   = $usertypeID;
        $this->data['subexams'] = $this->online_exam_m->get_online_exam_by_parent($id);
        $this->data['parentexam'] = $this->online_exam_m->get_single_online_exam(array('onlineExamID'=>$id));
        $this->data['online_exam_classes'] = $this->online_exam_class_m->get_classes($id);
        $this->data["subview"] = "online_exam/subexam/index";
        $this->load->view('_layout_main', $this->data);
	}

    protected function rules($type = 2, $ispaid = 0) {
        $rules = array(
            array(
                'field' => 'name',
                'label' => $this->lang->line("online_exam_name"),
                'rules' => 'trim|required|xss_clean|max_length[128]'
            ),
            array(
                'field' => 'description',
                'label' => $this->lang->line("online_exam_description"),
                'rules' => 'trim|xss_clean'
            ),
            // array(
            //     'field' => 'classes',
            //     'label' => $this->lang->line("online_exam_class"),
            //     'rules' => 'trim|xss_clean|required|numeric'
            // ),
            // array(
            //     'field' => 'section',
            //     'label' => $this->lang->line("online_exam_section"),
            //     'rules' => 'trim|xss_clean|required|numeric'
            // ),
            array(
                'field' => 'studentGroup',
                'label' => $this->lang->line("online_exam_studentGroup"),
                'rules' => 'trim|xss_clean|numeric'
            ),
            array(
                'field' => 'subject',
                'label' => $this->lang->line("online_exam_subject"),
                'rules' => 'trim|xss_clean|numeric'
            ),
            array(
                'field' => 'instruction',
                'label' => $this->lang->line("online_exam_instruction"),
                'rules' => 'trim|xss_clean|required|numeric'
            ),
            array(
                'field' => 'examStatus',
                'label' => $this->lang->line("online_exam_exam_status"),
                'rules' => 'trim|xss_clean|required|numeric|callback_unique_data'
            ),
            array(
                'field' => 'random',
                'label' => $this->lang->line("online_exam_random"),
				'rules' => 'trim|xss_clean|required|numeric|callback_unique_data'
            ),
            array(
                'field' => 'type',
                'label' => $this->lang->line("online_exam_type"),
                'rules' => 'trim|xss_clean|required|numeric|callback_unique_type'
            ),
            array(
                'field' => 'jenis',
                'label' => $this->lang->line("online_exam_jenis"),
                'rules' => 'trim|xss_clean|required|numeric|callback_unique_type'
            ),
            array(
                'field' => 'jenis_text',
                'label' => $this->lang->line("online_exam_jenis"),
                'rules' => 'trim|required|xss_clean|max_length[100]'
            ),
            array(
                'field' => 'duration',
                'label' => $this->lang->line("online_exam_duration"),
                'rules' => 'trim|xss_clean|numeric'
            ),
            array(
                'field' => 'markType',
                'label' => $this->lang->line("online_exam_markType"),
                'rules' => 'trim|required|xss_clean|numeric|callback_unique_markType'
            ),
            array(
                'field' => 'percentage',
                'label' => $this->lang->line("online_exam_percentage"),
                'rules' => 'trim|required|xss_clean|numeric'
            ),
            array(
                'field' => 'negativeMark',
                'label' => $this->lang->line("online_exam_negativeMark"),
                'rules' => 'trim|xss_clean|numeric'
            ),
            array(
                'field' => 'point',
                'label' => $this->lang->line("online_exam_point"),
                'rules' => 'trim|xss_clean|numeric'
            ),
            array(
                'field' => 'ispaid',
                'label' => $this->lang->line("online_exam_payment_status"),
                'rules' => 'trim|xss_clean|numeric|callback_ispaid_unique_data'
            ),
            array(
                'field' => 'published',
                'label' => $this->lang->line("online_exam_published"),
                'rules' => 'trim|xss_clean|required|numeric|callback_unique_data'
            ),

        );

        if($ispaid == 1) {
            $rules[] = array(
                'field' => 'cost',
                'label' => $this->lang->line("online_exam_cost"),
                'rules' => 'trim|xss_clean|required|numeric|callback_unique_cost'
            );
        }

        if($type == 4) {
            $rules[] = array(
                'field' => 'startdate',
                'label' => $this->lang->line("online_exam_startdatetime"),
                'rules' => 'trim|required|xss_clean|max_length[128]|callback_unique_date'
            );
            $rules[] = array(
                'field' => 'enddate',
                'label' => $this->lang->line("online_exam_enddatetime"),
                'rules' => 'trim|required|xss_clean|max_length[128]|callback_unique_date'
            );
        } elseif($type == 5) {
            $rules[] = array(
                'field' => 'startdatetime',
                'label' => $this->lang->line("online_exam_startdatetime"),
                'rules' => 'trim|required|xss_clean|max_length[128]|callback_unique_date_time'
            ); 
            $rules[] = array(
                'field' => 'enddatetime',
                'label' => $this->lang->line("online_exam_enddatetime"),
                'rules' => 'trim|required|xss_clean|max_length[128]|callback_unique_date_time'
            );
        }
        return $rules;
    }

    
	protected function subrules($type = 2, $ispaid = 0) {
        $subrules = array(
            array(
                'field' => 'name',
                'label' => $this->lang->line("online_exam_name"),
                'rules' => 'trim|required|xss_clean|max_length[128]'
            ),

            array(
                'field' => 'instruction',
                'label' => $this->lang->line("online_exam_instruction"),
                'rules' => 'trim|xss_clean|required|numeric'
            ),
           
            array(
                'field' => 'published',
                'label' => $this->lang->line("online_exam_published"),
                //'rules' => 'trim|xss_clean|required|numeric|callback_unique_data|callback_check_exam_question'
                'rules' => 'trim|xss_clean|required|numeric|callback_unique_data'
            ),

        );

        return $subrules;
    }

    
	
	public function unique_data($data) {
        if($data != "") {
            if($data === "0") {
                $this->form_validation->set_message('unique_data', 'The %s field is required.');
                return FALSE;
            }
            return TRUE;
        } 
        return TRUE;
    }

    public function ispaid_unique_data($data) {
        if($data != "") {
            if($data === "5") {
                $this->form_validation->set_message('ispaid_unique_data', 'The %s field is required.');
                return FALSE;
            }
            return TRUE;
        } 
        return TRUE;
    }

    public function unique_cost($data) {
        if($data != '') {
            if($data <= 0) {
                $this->form_validation->set_message('unique_cost', 'The %s field does not take zero value.');
                return FALSE;
            }
            return true;
        } 
        return true;
    }

    public function unique_date() {
        $startdate = $this->input->post('startdate');
        $enddate   = $this->input->post('enddate');
        if($startdate != '' && $enddate != '') {
            if(strtotime($startdate) > strtotime($enddate)) {
                $this->form_validation->set_message("unique_date", "The start date can not be upper than enddate.");
                return FALSE;
            }
        }
        return TRUE;
    }

    public function unique_date_time() {
        $startdatetime = $this->input->post('startdatetime');
        $enddatetime   = $this->input->post('enddatetime');
        if($startdatetime != '' && $enddatetime != '') {
            if(strtotime($startdatetime) > strtotime($enddatetime)) {
                $this->form_validation->set_message("unique_date_time", "The start date time can not be upper than end date time.");
                return FALSE;
            }
        }
        return TRUE;
    }

    public function add() {
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/datetimepicker/datetimepicker.css',
                'assets/select2/css/select2.css',
                'assets/select2/css/select2-bootstrap.css',
            ),
            'js' => array(
                'assets/datetimepicker/moment.js',
                'assets/datetimepicker/datetimepicker.js',
                'assets/select2/select2.js'
            )
        );

        $usertypeID = $this->session->userdata('usertypeID');
        $loginuserID = $this->session->userdata('loginuserID');
        $schoolYearID = $this->data['siteinfos']->school_year;

        $this->data['classes'] = $this->classes_m->get_order_by_classes();
        $this->data['usertypes'] = $this->usertype_m->get_usertype();
        $this->data['instructions'] = $this->instruction_m->get_order_by_instruction();
        $this->data['types'] = $this->exam_type_m->get_order_by_exam_type(['status' => 1]);
        $this->data['jenis'] = $this->exam_jenis_m->get_order_by_exam_jenis(['status' => 1]);
        $this->data['groups'] = $this->studentgroup_m->get_order_by_studentgroup();
        $this->data['userTypeID'] = 3;
        $this->data['subjects'] = array();
        $this->data['sections_option'] = [];

        if($_POST) {
            $this->data['posttype'] = $this->input->post('type');
            $rules = $this->rules($this->data['posttype'], $this->input->post('ispaid'));
            
            // jenis exam: jenis lain
            if($this->input->post('jenis') != 6){
                foreach ($rules as $key => $rule) {
                    if($rule['field'] == "jenis_text")
                    array_splice($rules, $key, 1);
                }
            }
            
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE) {
                $tmpSections = $this->section_m->get_order_by_section(array('classesID' => $this->input->post('classes')));

                $classes = [];

                $tmpClass = $this->classes_m->general_get_order_by_classes(['classesID' => $this->input->post('classes')]);
                foreach ($tmpClass as $key => $value) {
                    $classes[$value->classesID] = $value->classes;
                }
                foreach($tmpSections as $key => $value){
                    $this->data['sections_option'][$value->sectionID] = ((isset($classes[$value->classesID]))? $classes[$value->classesID]." - " : " " ).$value->section;
                }

                $this->data['form_validation'] = validation_errors();
                $this->data["subview"] = "/online_exam/add";
                $this->load->view('_layout_main', $this->data);
            } else {
                $inputs = $this->input->post();
                $databasePair = [
                    'name' => 'name',
                    // 'description' => 'description',
                    'usertype' => 'userTypeID',
                    // 'studentGroup' => 'studentGroupID',
                    // 'subject' => 'subjectID',
                    'instruction' => 'instructionID',
                    'duration' => 'duration',
                    'type' => 'examTypeNumber',
                    'jenis' => 'examJenisNumber',
                    'jenis_text' => 'examJenisText',
                    'markType' => 'markType',
                    'percentage' => 'percentage',
                    'negativeMark' => 'negativeMark',
                    'point' => 'point',
                    // 'ispaid' => 'paid',
                    'validDays' => 'validDays',
                    // 'cost' => 'cost',
                    'judge' => 'judge'
                ];
                if($inputs['type'] == 4) {
                    $databasePair ['startdate'] = 'startDateTime';
                    $databasePair ['enddate'] = 'endDateTime';
                } elseif ($inputs['type'] == 5) {
                    $databasePair ['startdatetime'] = 'startDateTime';
                    $databasePair ['enddatetime'] = 'endDateTime';
                }

                $array = [];
                foreach ($databasePair as $key => $database) {
                    if($inputs[$key] != "") {
                        if($database == 'startDateTime' || $database == 'endDateTime') {
                            $array[$database] = date('Y-m-d H:i:s', strtotime($inputs[$key]));
                        } else {
                            $array[$database] = $inputs[$key];
                        }
                    }
                }

                if($inputs['jenis'] == 3 || $inputs['jenis'] == 6 || $inputs['jenis'] == 5){
                    $array['is_show_answer'] = $inputs['is_show_answer'];
                }

                // if($this->input->post('ispaid') == 0) {
                //     $array['cost'] = 0;
                // }

                $array['examStatus'] = $this->input->post('examStatus');
                $array['random'] = $this->input->post('random');
                $array['published']   = $this->input->post('published');
                $array['create_date'] = date("Y-m-d H:i:s");
                $array['modify_date'] = date("Y-m-d H:i:s");
                $array['create_userID'] =$usertypeID;
                $array['create_usertypeID'] = $loginuserID;
                $array['schoolYearID'] = $schoolYearID;
                $array['negativeMark'] = $this->input->post('negativeMark');
                $array['point'] = $this->input->post('point');
                $array['jenisExam'] = "parent";
				$array['parentID'] = 0;
				$array['paid'] = 0;
                $onlineExamId = $this->online_exam_m->insert_online_exam($array);
                if($onlineExamId) {
                    $this->online_exam_class_m->insert_for_online_exam($onlineExamId, $inputs['classes']);
                    $this->online_exam_section_m->insert_for_online_exam($onlineExamId, $inputs['sections']);
                }
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                redirect(base_url("online_exam/index"));
            }
        } else {
            $this->data['posttype'] = $this->input->post('type');
            $this->data["subview"] = "/online_exam/add";
            $this->load->view('_layout_main', $this->data);
        }
    }
	
	public function addsubexams($id=0) {
		$parentexam = $this->online_exam_m->get_single_online_exam(array('onlineExamID'=>$id));
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/datetimepicker/datetimepicker.css',
                'assets/select2/css/select2.css',
                'assets/select2/css/select2-bootstrap.css',
            ),
            'js' => array(
                'assets/datetimepicker/moment.js',
                'assets/datetimepicker/datetimepicker.js',
                'assets/select2/select2.js'
            )
        );

        $usertypeID = $this->session->userdata('usertypeID');
        $loginuserID = $this->session->userdata('loginuserID');
        $schoolYearID = $this->data['siteinfos']->school_year;

        $this->data['instructions'] = $this->instruction_m->get_order_by_instruction();
     
        $this->data['userTypeID'] = 3;

        if($_POST) {
           
            $this->data['posttype'] = $this->input->post('type');
            $subrules = $this->subrules($this->data['posttype'], $this->input->post('ispaid'));
            $this->form_validation->set_rules($subrules);
            if ($this->form_validation->run() == FALSE) {
                $this->data['form_validation'] = validation_errors();
                $this->data["subview"] = "/online_exam/subexam/add";
                $this->load->view('_layout_main', $this->data);
            } else {
                $array['name'] = $this->input->post('name');
				$array['instructionID'] = $this->input->post('instruction');

                $array['userTypeID'] = $parentexam->userTypeID;
                $array['sectionID'] = $parentexam->sectionID;
                $array['studentGroupID'] = $parentexam->studentGroupID;
                $array['subjectID'] = $parentexam->subjectID;
                $array['duration'] = $parentexam->duration;
                $array['examTypeNumber'] = $parentexam->examTypeNumber;
                $array['examJenisNumber'] = $parentexam->examJenisNumber;
                $array['markType'] = $parentexam->markType;
                $array['percentage'] = $parentexam->percentage;
                $array['negativeMark'] = $parentexam->negativeMark;
                $array['point'] = $parentexam->point;
                $array['paid'] = $parentexam->paid;
                $array['judge'] = $parentexam->judge;
                $array['validDays'] = $parentexam->validDays;
                $array['cost'] = $parentexam->cost;
				$array['examStatus'] = $parentexam->examStatus;
				$array['random'] = $parentexam->random;
				$array['is_show_answer'] = $parentexam->is_show_answer;
				
                $array['published']   = $this->input->post('published');
                $array['create_date'] = date("Y-m-d H:i:s");
                $array['modify_date'] = date("Y-m-d H:i:s");
                $array['create_userID'] =$usertypeID;
                $array['create_usertypeID'] = $loginuserID;
                $array['schoolYearID'] = $schoolYearID;
                $array['jenisExam'] = "child";
				$array['parentID'] = $id;
                $onlineExamId = $this->online_exam_m->insert_online_exam($array);
                if($onlineExamId) {
                    $classesId = [];
                    $sectionsId = [];
                    $parentClasses = $this->online_exam_class_m->get_by_online_exam($id);
                    $onlineExamSections = $this->online_exam_section_m->get_by_online_exam($id);

                    foreach ($parentClasses as $value) {
                        $classesId[] = $value->classID;
                    }
                    foreach ($onlineExamSections as $value) {
                        $sectionsId[] = $value->sectionID;
                    }
                    $this->online_exam_class_m->insert_for_online_exam($onlineExamId, $classesId);
                    $this->online_exam_section_m->insert_for_online_exam($onlineExamId, $sectionsId);
                }
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                redirect(base_url("online_exam/subexam/{$id}"));
            }
        } else {
            $this->data['posttype'] = $this->input->post('type');
            $this->data["subview"] = "/online_exam/subexam/add";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function edit() {
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/datetimepicker/datetimepicker.css',
                'assets/select2/css/select2.css',
                'assets/select2/css/select2-bootstrap.css',
            ),
            'js' => array(
                'assets/datetimepicker/moment.js',
                'assets/datetimepicker/datetimepicker.js',
                'assets/select2/select2.js'
            )
        );
        $id = htmlentities(escapeString($this->uri->segment(3)));
        if((int)$id) {
            $this->data['online_exam'] = $this->online_exam_m->get_single_online_exam(array('onlineExamID' => $id));
            if($this->data['online_exam']) {
                $selectedClassesId = [];
                $selectedSectionsId = [];
                if($_POST){
                    if($this->input->post('classes'))
                        $selectedClassesId = $this->input->post('classes');
                    if($this->input->post('sections'))
                        $selectedSectionsId = $this->input->post('sections');
                } else {
                    $onlineExamClasses = $this->online_exam_class_m->get_by_online_exam($id);
                    $onlineExamSections = $this->online_exam_section_m->get_by_online_exam($id);
                    foreach ($onlineExamClasses as $value) {
                        $selectedClassesId[] = $value->classID;
                    }
                    foreach ($onlineExamSections as $value) {
                        $selectedSectionsId[] = $value->sectionID;
                    }
                }

                $this->data['classes'] = $this->classes_m->get_join_classes();
                $this->data['selected_classes'] = $selectedClassesId;
                $this->data['usertypes'] = $this->usertype_m->get_usertype();
                $this->data['instructions'] = $this->instruction_m->get_order_by_instruction();
                $this->data['types'] = $this->exam_type_m->get_order_by_exam_type(['status' => 1]);
                $this->data['jenis'] = $this->exam_jenis_m->get_order_by_exam_jenis(['status' => 1]);
                $this->data['groups'] = $this->studentgroup_m->get_order_by_studentgroup();
                $this->data['subjects'] = [];
                $this->data['sections_option'] = [];
                $this->data['selected_sections'] = $selectedSectionsId;

                if(count($selectedClassesId)) {
                    $this->data['subjects'] = $this->subject_m->get_order_by_subject(array('classesID' => $selectedClassesId));

                    $classes = [];
                    $tmpSections = $this->section_m->get_order_by_section(array('classesID' => $selectedClassesId));
    
                    $tmpClass = $this->classes_m->general_get_order_by_classes(['classesID' => $selectedClassesId]);
                    foreach ($tmpClass as $key => $value) {
                        $classes[$value->classesID] = $value->classes;
                    }
                    foreach($tmpSections as $key => $value){
                        $this->data['sections_option'][$value->sectionID] = ((isset($classes[$value->classesID]))? $classes[$value->classesID]." - " : " " ).$value->section;
                    }
                }
                $this->data['userTypeID'] = 3;
                if($_POST) {
                    $this->data['posttype'] = $this->input->post('type');
                    $rules = $this->rules($this->data['posttype'], $this->input->post('ispaid'));
                    
                    // $jenis_exam = intval($this->input->post('jenis'));

                    // jenis exam: jenis lain
                    if($this->input->post('jenis') != 6){
                        foreach ($rules as $key => $rule) {
                            if($rule['field'] == "jenis_text")
                                array_splice($rules, $key, 1);
                        }
                    }

                    $this->form_validation->set_rules($rules);
                    if ($this->form_validation->run() == FALSE) {
                        $this->data['sections'] = $this->section_m->get_order_by_section(array('classesID' => $this->input->post('classes')));
                        $this->data['subjects'] = $this->subject_m->get_order_by_subject(array('classesID' => $this->input->post('classes')));

                        $this->data["subview"] = "/online_exam/edit";
                        $this->load->view('_layout_main', $this->data);
                    } else {
                        $inputs = $this->input->post();
                        $databasePair = [
                            'name' => 'name',
                            'description' => 'description',
                            'usertype' => 'userTypeID',
                            // 'studentGroup' => 'studentGroupID',
                            // 'subject' => 'subjectID',
                            'instruction' => 'instructionID',
                            'duration' => 'duration',
                            'type' => 'examTypeNumber',
                            'jenis' => 'examJenisNumber',
                            'jenis_text' => 'examJenisText',
                            'markType' => 'markType',
                            'negativeMark' => 'negativeMark',
                            'point' => 'point',
                            'percentage' => 'percentage',
                            // 'ispaid' => 'paid',
                            'validDays' => 'validDays',
                            // 'cost' => 'cost',
                            'judge' => 'judge'
                        ];
                        if($inputs['type'] == 4) {
                            $databasePair ['startdate'] = 'startDateTime';
                            $databasePair ['enddate'] = 'endDateTime';
                        } elseif ($inputs['type'] == 5) {
                            $databasePair ['startdatetime'] = 'startDateTime';
                            $databasePair ['enddatetime'] = 'endDateTime';
                        }

                        $array = [];
                        $f = 1;
                        foreach ($databasePair as $key => $database) {
                            if($inputs[$key] != "") {
                                if($database == 'startDateTime' || $database == 'endDateTime') {
                                    $f = 0;
                                    $array[$database] = date('Y-m-d H:i:s', strtotime($inputs[$key]));
                                } else {
                                    $array[$database] = $inputs[$key];
                                }
                            }
                        }
                        if($f) {
                            $array['startDateTime'] = NULL;
                            $array['endDateTime'] = NULL;
                        }

                        if($this->input->post('ispaid') == 0) {
                            $array['cost'] = 0;
                        }
                        

                        if($inputs['jenis'] == 3 || $inputs['jenis'] == 6 || $inputs['jenis'] == 5){
                            $array['is_show_answer'] = $inputs['is_show_answer'];
                        } else {
                            $array['is_show_answer'] = 0;
                        }

                        $array['examStatus'] = $this->input->post('examStatus');
                        $array['random'] = $this->input->post('random');
						$array['jenisExam'] = "parent";
						$array['parentID'] = 0;
                        $array['published']   = $this->input->post('published');
                        $array['modify_date'] = date("Y-m-d H:i:s");

                        $this->online_exam_m->update_online_exam($array, $id);
						
                        // jenis exam: sikap kerja
						if($this->data['online_exam']->examJenisNumber == 5) {
							 $online_examparent = $this->online_exam_m->get_single_online_exam(array('onlineExamID' => $id));
							 $subexams = xGetRows("select * from online_exam where jenisExam = 'child' and parentID = {$id}");
							 foreach($subexams as $sub){
								$sarrays['sectionID'] = $online_examparent->sectionID;
								$sarrays['studentGroupID'] = $online_examparent->studentGroupID;
								$sarrays['subjectID'] = $online_examparent->subjectID;
								$sarrays['duration'] = $online_examparent->duration;
								$sarrays['examTypeNumber'] = $online_examparent->examTypeNumber;
								$sarrays['examJenisNumber'] = $online_examparent->examJenisNumber;
								$sarrays['markType'] = $online_examparent->markType;
								$sarrays['percentage'] = $online_examparent->percentage;
								$sarrays['negativeMark'] = $online_examparent->negativeMark;
								$sarrays['point'] = $online_examparent->point;
								$sarrays['paid'] = $online_examparent->paid;
								$sarrays['judge'] = $online_examparent->judge;
								$sarrays['validDays'] = $online_examparent->validDays;
								$sarrays['cost'] = $online_examparent->cost;
								$sarrays['examStatus'] = $online_examparent->examStatus;
								$sarrays['random'] = $online_examparent->random;
								$sarrays['is_show_answer'] = $online_examparent->is_show_answer;
								
								xUpdateTable("online_exam",$sarrays," onlineExamID = {$sub['onlineExamID']}"); 
							 }
						}
						
                        $this->online_exam_class_m->update_classes($id, $selectedClassesId);
                        $this->online_exam_section_m->update_sections($id, $selectedSectionsId);

                        $examChildren = $this->online_exam_m->get_online_exam_by_parent($id);
                        foreach ($examChildren as $key => $value) {
                            $childId = $value->onlineExamID;
                            $this->online_exam_class_m->update_classes($childId, $selectedClassesId);
                            $this->online_exam_section_m->update_sections($childId, $selectedSectionsId);
                        }
                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                        redirect(base_url("online_exam/index"));
                    }
                } else {
                    $this->data['posttype'] = $this->data['online_exam']->examTypeNumber;
                    $this->data['postjenis'] = $this->data['online_exam']->examJenisNumber;
                    $this->data["subview"] = "/online_exam/edit";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }
	
	public function editsubexam() {
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/datetimepicker/datetimepicker.css',
                'assets/select2/css/select2.css',
                'assets/select2/css/select2-bootstrap.css',
            ),
            'js' => array(
                'assets/datetimepicker/moment.js',
                'assets/datetimepicker/datetimepicker.js',
                'assets/select2/select2.js'
            )
        );
		$usertypeID = $this->session->userdata('usertypeID');
        $loginuserID = $this->session->userdata('loginuserID');
        $schoolYearID = $this->data['siteinfos']->school_year;
		
        $id = htmlentities(escapeString($this->uri->segment(3)));
		$subexamdata = $this->online_exam_m->get_single_online_exam(array('onlineExamID'=>$id));
		$parentexam = $this->online_exam_m->get_single_online_exam(array('onlineExamID'=>$subexamdata->parentID));
        if((int)$id) {
            $this->data['online_exam'] = $this->online_exam_m->get_single_online_exam(array('onlineExamID' => $id));
            if($this->data['online_exam']) {
                
                $this->data['instructions'] = $this->instruction_m->get_order_by_instruction();
               
                if(isset($this->data['online_exam']->classID)) {
                    $this->data['sections'] = $this->section_m->get_order_by_section(array('classesID' => $this->data['online_exam']->classID));
                    $this->data['subjects'] = $this->subject_m->get_order_by_subject(array('classesID' => $this->data['online_exam']->classID));
                }
                $this->data['userTypeID'] = 3;
                if($_POST) {
                    $this->data['sections'] = $this->section_m->get_order_by_section(array('classesID' => $this->input->post('classes')));
                    $this->data['subjects'] = $this->subject_m->get_order_by_subject(array('classesID' => $this->input->post('classes')));
                    $this->data['posttype'] = $this->input->post('type');
                    $subrules = $this->subrules($this->data['posttype'], $this->input->post('ispaid'));
                    $this->form_validation->set_rules($subrules); 
                    if ($this->form_validation->run() == FALSE) {
                        $this->data["subview"] = "/online_exam/subexam/edit";
                        $this->load->view('_layout_main', $this->data);
                    } else {
                        $array['name'] = $this->input->post('name');
						$array['instructionID'] = $this->input->post('instruction');
						$array['description'] = $this->input->post('description');

						$array['userTypeID'] = $usertypeID;
						$array['classID'] = $parentexam->classID;
						$array['sectionID'] = $parentexam->sectionID;
						$array['studentGroupID'] = $parentexam->studentGroupID;
						$array['subjectID'] = $parentexam->subjectID;
						$array['duration'] = $parentexam->duration;
						$array['examTypeNumber'] = $parentexam->examTypeNumber;
						$array['examJenisNumber'] = $parentexam->examJenisNumber;
						$array['markType'] = $parentexam->markType;
						$array['percentage'] = $parentexam->percentage;
						$array['negativeMark'] = $parentexam->negativeMark;
						$array['point'] = $parentexam->point;
						$array['paid'] = $parentexam->paid;
						$array['judge'] = $parentexam->judge;
						$array['validDays'] = $parentexam->validDays;
						$array['cost'] = $parentexam->cost;
						$array['examStatus'] = $parentexam->examStatus;
						$array['random'] = $parentexam->random;
						
						$array['published']   = $this->input->post('published');
						$array['create_date'] = date("Y-m-d H:i:s");
						$array['modify_date'] = date("Y-m-d H:i:s");
						$array['create_userID'] =$usertypeID;
						$array['create_usertypeID'] = $loginuserID;
						$array['schoolYearID'] = $schoolYearID;
						$array['jenisExam'] = "child";
						$array['parentID'] = $subexamdata->parentID;

                        $this->online_exam_m->update_online_exam($array, $id);
                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                        redirect(base_url("online_exam/subexam/".$subexamdata->parentID.""));
						
						//print_r($this->db->last_query());    
                    }
                } else {
                    $this->data['posttype'] = $this->data['online_exam']->examTypeNumber;
                    $this->data['postjenis'] = $this->data['online_exam']->examJenisNumber;
                    $this->data["subview"] = "online_exam/subexam/edit";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function delete() {
        $id = htmlentities(escapeString($this->uri->segment(3)));
        if((int)$id) {
            $this->data['online_exam'] = $this->online_exam_m->get_single_online_exam(array('onlineExamID' => $id));
            if($this->data['online_exam']) {
                $this->online_exam_m->delete_online_exam($id);
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                redirect(base_url("online_exam/index"));
            } else {
                redirect(base_url("online_exam/index"));
            }
        } else {
            redirect(base_url("online_exam/index"));
        }
    }

    public function addquestion() {
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/checkbox/checkbox.css',
            )
        );
        $onlineExamID = htmlentities(escapeString($this->uri->segment(3)));

        $this->data['onlineExamID'] = $onlineExamID;
        $this->addQuestionDatabase(true, $onlineExamID);
        $this->data['levels'] = $this->question_level_m->get_order_by_question_level();
        $this->data['groups'] = $this->question_group_m->get_order_by_question_group();
        $onlineExam = $this->online_exam_m->get_single_online_exam(['onlineExamID' => $onlineExamID]);
        if(!is_null($onlineExam)) {
            $this->data['class'] = $this->classes_m->get_classes($onlineExam->classID);
            $this->data['section'] = $this->section_m->get_section($onlineExam->sectionID);
            $this->data['studentGroup'] = $this->studentgroup_m->get_studentgroup($onlineExam->studentGroupID);
            $this->data['instruction'] = $this->instruction_m->get_instruction($onlineExam->instructionID);
            $this->data['examType'] = $this->exam_type_m->get_single_exam_type(['examTypeNumber' => $onlineExam->examTypeNumber]);
            $this->data['examJenis'] = $this->exam_jenis_m->get_single_exam_jenis(['examJenisNumber' => $onlineExam->examJenisNumber]);
            $this->data['subject'] = $this->subject_m->get_subject($onlineExam->subjectID);
        }
        $this->data['onlineExam'] = $onlineExam;
        $this->data["subview"] = "/online_exam/addquestion";
        $this->load->view('_layout_main', $this->data);
    }
	
	public function addsubquiz() {
        $this->data['headerassets'] = array(
            'css' => array(
                'assets/checkbox/checkbox.css',
            )
        );
        $onlineExamID = htmlentities(escapeString($this->uri->segment(3)));

        $this->data['onlineExamID'] = $onlineExamID;
        $this->addQuestionDatabase(true, $onlineExamID);
        $this->data['levels'] = $this->question_level_m->get_order_by_question_level();
        $this->data['groups'] = $this->question_group_m->get_order_by_question_group();
        $onlineExam = $this->online_exam_m->get_single_online_exam(['onlineExamID' => $onlineExamID]);
        if(!is_null($onlineExam)) {
            $this->data['class'] = $this->classes_m->get_classes($onlineExam->classID);
            $this->data['section'] = $this->section_m->get_section($onlineExam->sectionID);
            $this->data['studentGroup'] = $this->studentgroup_m->get_studentgroup($onlineExam->studentGroupID);
            $this->data['instruction'] = $this->instruction_m->get_instruction($onlineExam->instructionID);
            $this->data['examType'] = $this->exam_type_m->get_single_exam_type(['examTypeNumber' => $onlineExam->examTypeNumber]);
            $this->data['examJenis'] = $this->exam_jenis_m->get_single_exam_jenis(['examJenisNumber' => $onlineExam->examJenisNumber]);
            $this->data['subject'] = $this->subject_m->get_subject($onlineExam->subjectID);
        }
        $this->data['onlineExam'] = $onlineExam;
        $this->data["subview"] = "/online_exam/addsubquiz";
        $this->load->view('_layout_main', $this->data);
    }

    // TODO [ggg]: check this function
    public function showQuestions() {
        $inputs = $this->input->post();
        $where = [];
        if($inputs['levelID']) {
            $where['levelID'] = $inputs['levelID'];
        }
        if($inputs['groupID']) {
            $where['groupID'] = $inputs['groupID'];
        }
        $this->data['questions'] = $this->question_bank_m->get_order_by_question_bank($where);
        $this->data['types'] = pluck($this->question_type_m->get_order_by_question_type(), 'obj', 'typeNumber');

        echo $this->load->view('/online_exam/questionList', $this->data, true);
    }

    public function addQuestionDatabase($initial = false, $onlineExamID = 0, $questionID = 0) {
        if(!$initial) {
            $onlineExamID = $this->input->post('onlineExamID');
            $questionID = $this->input->post('questionID');
            $haveExamQuestion = $this->online_exam_question_m->get_order_by_online_exam_question([
                'onlineExamID' => $onlineExamID,
                'questionID' => $questionID
            ]);
            if(!inicompute($haveExamQuestion)) {
                $this->online_exam_question_m->insert_online_exam_question([
                    'onlineExamID' => $onlineExamID,
                    'questionID' => $questionID
                ]);
            }
        }

        $this->data['onlineExamQuestions'] = $this->online_exam_question_m->get_order_by_online_exam_question([
            'onlineExamID' => $onlineExamID
        ]);
        $this->data['questions'] = pluck($this->question_bank_m->get_order_by_question_bank(), 'obj', 'questionBankID');
        $allOptions = $this->question_option_m->get_order_by_question_option();
        $options = [];
        foreach ($allOptions as $option) {
            if($option->name == "" && $option->img == "") continue;
            $options[$option->questionID][] = $option;
        }
        $this->data['options'] = $options;
        $allAnswers = $this->question_answer_m->get_order_by_question_answer();
        $answers = [];
        foreach ($allAnswers as $answer) {
            $answers[$answer->questionID][] = $answer;
        }
        $this->data['answers'] = $answers;
        $showArray['associateQuestionList'] = $this->load->view('/online_exam/associateQuestionList', $this->data, true);
        $showArray['questionSummary'] = $this->load->view('/online_exam/questionSummary', $this->data, true);
        $this->data['associateQuestionList'] = $showArray['associateQuestionList'];
        $this->data['questionSummary'] = $showArray['questionSummary'];
        $this->data['updateView'] = $showArray;
        if(!$initial) {
            echo json_encode($showArray);
        }
    }

    public function removeQuestionDatabase() {
        $onlineExamQuestionID = $this->input->post('onlineExamQuestionID');
        $onlineExamID = $this->input->post('onlineExamID');
        $this->online_exam_question_m->delete_online_exam_question($onlineExamQuestionID);
        $this->addQuestionDatabase(true, $onlineExamID);
        echo json_encode($this->data['updateView']);
    }

    public function getSection() {
        $id = $this->input->post('id');
        if($id) {
            $classes = [];

            $tmpClass = $this->classes_m->general_get_order_by_classes(['classesID' => $id]);
            foreach ($tmpClass as $key => $value) {
                $classes[$value->classesID] = $value->classes;
            }

            $allSection = $this->section_m->get_order_by_section(array('classesID' => $id));

            foreach ($allSection as $value) {
                echo "<option value=\"$value->sectionID\">".((isset($classes[$value->classesID]))? $classes[$value->classesID]."-" : "" ).$value->section."</option>";
            }
        }
    }

    public function getSubject() {
        $classID = $this->input->post('classID');
        if((int)$classID) {
            $allSubject = $this->subject_m->get_order_by_subject(array('classesID' => $classID));

            echo "<option value=''>", $this->lang->line("online_exam_select"),"</option>";

            foreach ($allSubject as $value) {
                echo "<option value=\"$value->subjectID\">",$value->subject,"</option>";
            }

        }
    }

    public function unique_type() {
        if($this->input->post('type') == 0) {
            $this->form_validation->set_message("unique_type", "The %s field is required.");
            return FALSE;
        }
        return TRUE;
    }

    public function unique_markType() {
        if($this->input->post('markType') == 0) {
            $this->form_validation->set_message("unique_markType", "The %s field is required.");
            return FALSE;
        }
        return TRUE;
    }

    public function unique_section() {
        if($this->input->post('classes')) {
            if($this->input->post('section') == 0) {
                $this->form_validation->set_message("unique_section", "The %s field is required.");
                return FALSE;
            }
            return TRUE;
        }
        return TRUE;
    }

    public function check_exam_question() {
        $onlineexamID = htmlentities(escapeString($this->uri->segment(3)));
        $published = $this->input->post('published');
        if((int)$onlineexamID) {
            $online_exam_questions = $this->online_exam_question_m->get_order_by_online_exam_question(array('onlineExamID'=>$onlineexamID));
            if((inicompute($online_exam_questions) == 0) && ($published == 1)) {
                $this->form_validation->set_message("check_exam_question", "Please add some question and publish this exam.");
                return FALSE;
            }
            return TRUE;
        } else {
            if($published == 1) {
                $this->form_validation->set_message("check_exam_question", "Please add some question and publish this exam.");
                return FALSE;
            }
            return TRUE;
        }
    }

}
