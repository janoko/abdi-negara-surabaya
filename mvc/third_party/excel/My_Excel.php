<?php
require_once APPPATH.'/../Libs/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class My_Excel {
    const TemplatePath = APPPATH.'/third_party/excel/templates/';
    const OutputPath = APPPATH.'/third_party/excel/output/';

    static function DefaultStyle($spreadsheet) {
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    }

    static function ExamReport($dataReports) {
        $spreadsheet = IOFactory::load(My_Excel::TemplatePath."exam-report.xlsx");
        
        My_Excel::DefaultStyle($spreadsheet);

        $sheet = $spreadsheet->getActiveSheet();
        
        $i = 0;
        foreach ($dataReports as $report) {
            $i++;
            $sheet->setCellValue('A'.($i+1), $i);
            $sheet->setCellValue('B'.($i+1), $report['name']);
            $sheet->setCellValue('C'.($i+1), $report['datetime']);
            $sheet->setCellValue('D'.($i+1), $report['totalMark']);
            $sheet->setCellValue('E'.($i+1), $report['totalPresentage']);
            $sheet->setCellValue('F'.($i+1), $report['status']);

            $sheet->getStyle('E'.($i+1))
                ->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);
        }

        $writer = new Xlsx($spreadsheet);
        
        return $writer;
    }
}