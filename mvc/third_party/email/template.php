<!DOCTYPE html>
<html>

<head>
  <title></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <style type="text/css">
    /* RESET STYLES */
    body {
      height: 100% !important;
      margin: 0 !important;
      padding: 0 !important;
      width: 100% !important;
      background-color: #f4f4f4; 
      margin: 0 !important; 
      padding: 0 !important;
      color: #303030;
    }
    
    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
    }

    /* Layout */

    .my-email {
      background-color: #cacaca; 
      color: #303030;
      font-family: 'Lato', Helvetica, Arial, sans-serif; 
      padding-bottom: 2px;
    }
    .container-md {
      max-width: 520px;
      margin: 0px auto;
    }

    .header {
      background-color: #1f1f1f;
      text-align: center;
      padding-top: 39px;
      padding-bottom: 23px;
    }
    .header-logo {
      width: 100%;
    }
    .header-logo img {
      width: 160px;
      display: inline-block;
    }
    .header-title {
      background-color: #1f1f1f;
      color: #767676;
    }
    .header-title h1, .header-title h2 {
      margin: 0px;
    }
    .header-title h1 {
      color: #dcdcdc;
    }
    .content {
      padding: 20px 32px;
      background-color: #ffffff;
    }
    .footer {
      padding: 20px 32px;
    }
    .footer-warning {
      background-color: #fff1d3;
      margin: 20px auto;
    }

    .text-center {
      text-align: center;
    }

    /* Asset */
    .btn {
      font-size: 20px; 
      text-decoration: none; 
      padding: 15px 25px; 
      border-radius: 2px; 
      display: inline-block;
      background-color: rgb(73, 163, 163);
      color: rgb(255, 255, 255);
      border: none;
      cursor: pointer;
    }
    .btn:hover {
      background-color: rgb(49, 134, 134);
      color: rgb(202, 202, 202);
    }
    .code-xl {
      font-size: 26pt;
      color: rgb(49, 134, 134);
    }
  </style>
</head>

<body>
  <div class="my-email">
    <div class="container-md">
      <div class="header">
        <div class="header-logo">
          <img src="https://member.abdinegarasurabaya.id/uploads/images/eb82f95da5c0cd0d94d32c1fcb989e34a4c5c610a6e166ca2f0b1a0465609915e8c8b81091bae8b3b75dba887d1ddc88b799af400ae338878277814b754f4482.png" width="125" height="120" />
        </div>
        <div class="header-title">
          <h2>Abdi Negara Surabaya</h2>
          <h1>Kode Verifikasi</h1>
        </div>
      </div>
      <div class="content">
        <p>
          Anda telah login ke member.abdinegarasurabaya.id.
        </p>
        <p>
          Untuk melanjutkan proses verifikasi silahkan menggunakan kode dibawah ini.
        </p>
        <div class="code-xl text-center" style="margin: 60px auto;">
          <?= $code ?>
        </div>
        <p>
          Kode ini hanya bisa digunakan 24 jam untuk 1 akun. Mohon untuk merahasiakan kode ini.
        </p>
        <p>
          Terimakasih,
        </p>
        <p>
          Team
        </p>
      </div>
      <div class="footer text-center">
        member.abdinegarasurabaya.id
      </div>
    </div>
  </div>
</body>
</html>