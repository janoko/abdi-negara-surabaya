<?php
require_once APPPATH.'/../Libs/vendor/autoload.php';

class My_Email {
  static public function sendVerificationCode($email, $code) {
    ob_start();
    include(APPPATH.'/third_party/email/template.php');
    $emailBody = ob_get_contents();
    ob_end_clean();
    
    // Create the Transport
    $transport = (new Swift_SmtpTransport('abdinegarasurabaya.id', 587, 'tls'))
      ->setUsername('admin@abdinegarasurabaya.id')
      ->setPassword('password')
    ;
    $mailer = new Swift_Mailer($transport);
    $message = (new Swift_Message('Verification'))
      ->setFrom(['admin@abdinegarasurabaya.id' => 'Abdi Negara Surabaya'])
      ->setTo([$email])
      ->setBody($emailBody, 'text/html');
    
    // Send the message
    $result = $mailer->send($message);
  }
}